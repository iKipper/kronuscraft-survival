package com.kronuscraftmc.misc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

public class VoteParty {

	private final int votesStarter = 30;
	private int votes;
	private FileConfiguration file = Survival.getInstance().getConfig();
	
	public VoteParty() {
		this.votes = file.getInt("Votes");
	}
	
	public int getRemainingVotes() {
		return votesStarter - votes;
	}
	
	public void addVote() {
		votes++;
		if(votes == votesStarter) {
			votePartySetup();
		}
		file.set("Votes", votes);
		Survival.getInstance().saveConfig();
	}
	
	private void votePartySetup() {
		votes = 0;
		for(Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage("");
			player.sendMessage("");
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lThe Drop Party will be triggered in 1 minute!"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fRewards given to all:"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x10 Coal Blocks"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x10 Iron Blocks"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x8 Gold Blocks"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x4 Emerald Blocks"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x3 Diamond blocks"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x2 Voter Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x1 Mystery Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x1 Slimefun Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + 2 Vote Credits"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + $10,000 In-Game Money"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&oEnsure inventory has space!"));
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					if(!player.getWorld().getName().equals("Competition")) {
						SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " voter 2");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " mystery 1");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " slimefun 1");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getPlayer().getName() + " 10000");
						ItemStack[] items = { new ItemStack(Material.COAL_BLOCK, 10), new ItemStack(Material.IRON_BLOCK, 10), new ItemStack(Material.GOLD_BLOCK, 8),
								new ItemStack(Material.DIAMOND_BLOCK, 3), new ItemStack(Material.EMERALD_BLOCK, 4)};
						for(ItemStack is : items) {
							player.getInventory().addItem(is);
						}
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fYour Vote Party rewards have been awarded!"));
						if(sPlayer != null) {
							sPlayer.getGamePlayer().setVoteCredits(sPlayer.getGamePlayer().getVoteCredits() + 2);
						}	
					}
				}
			}
		}, 20 * 60);
	}
}
