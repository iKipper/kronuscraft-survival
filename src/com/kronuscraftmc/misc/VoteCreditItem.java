package com.kronuscraftmc.misc;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.player.SurvivalPlayerManager;

public class VoteCreditItem implements Listener, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if(s.isOp()) {
			int money = Integer.valueOf(args[0]);
			Player p = (Player) s;
			p.getInventory().addItem(createTokenItem(money));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fGiven Vote Credit Token &a" + money + " Vote Credit(s)&f!"));
		}
		return true;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getInventory().getItemInMainHand().getType() == Material.BOOK) {
				ItemStack is = e.getPlayer().getInventory().getItemInMainHand();
				if(is.getItemMeta().getDisplayName().startsWith(ChatColor.RED + "Vote Credits " + ChatColor.GRAY + "Tokens: ")) {
					String splitted = is.getItemMeta().getDisplayName().replace(ChatColor.RED + "Vote Credits " + ChatColor.GRAY + "Tokens: ", "");
					 int amount = Integer.parseInt(splitted);
					 if(is.getAmount() > 1) {
						is.setAmount(is.getAmount() - 1);
					 } else {
						 e.getPlayer().getInventory().removeItem(is);
					 }
					 SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId()).getGamePlayer().setVoteCredits(SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId()).getGamePlayer().getVoteCredits() + amount);
					 SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId()).setupScoreboard();
					 e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fYou have received &a" + amount + " Vote Credit(s)&f!"));
				}
			}
		}
	}
	
	public ItemStack createTokenItem(int amount) {
		ItemStack is = new ItemStack(Material.BOOK);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Vote Credits " + ChatColor.GRAY + "Tokens: " + amount);
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "DO NOT STACK");
		is.setItemMeta(im);
		return is;
	}
}