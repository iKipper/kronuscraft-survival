package com.kronuscraftmc.misc;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.shops.BuyableItem;
import com.kronuscraftmc.shops.ShopConfig;

public class SellWand implements Listener, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if(s.isOp()) {
			int uses = Integer.valueOf(args[0]);
			Player p = (Player) s;
			p.getInventory().addItem(createWandItem(uses));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fGiven you a &aSell Wand " + uses + "&f!"));
		}
		return true;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getInventory().getItemInMainHand().getType() == Material.BLAZE_ROD) {
				if(e.getClickedBlock().getType() == Material.CHEST || e.getClickedBlock().getType() == Material.TRAPPED_CHEST) {
					ItemStack is = e.getPlayer().getInventory().getItemInMainHand();
					String name = is.getItemMeta().getDisplayName();
					if (name.startsWith(ChatColor.RED + "Sell Wand " + ChatColor.GRAY + "Uses: ")) {
						if (Survival.getInstance().getGriefPrevention().allowBuild(e.getPlayer(),e.getClickedBlock().getLocation()) == null) {
							e.setCancelled(true);
							Chest chest = (Chest) e.getClickedBlock().getState();
							for (ShopConfig shops : Survival.getInstance().getShopManager().getShopConfigs()) {
								if (shops != null) {
									BuyableItem[] buyItems = shops.getItems();
									for (BuyableItem buyItem : buyItems) {
										buyAll(e.getPlayer(), chest, buyItem);
									}
								}
							}
							String splitted = name.replace(ChatColor.RED + "Sell Wand " + ChatColor.GRAY + "Uses: ","");
							e.getPlayer().getInventory().setItemInMainHand(createWandItem(Integer.parseInt(splitted) - 1));
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fSell Wand used!"));
							if(Integer.valueOf(splitted) <= 1) {
								e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
								e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fSell Wand has broken!"));
							}
						}
					}	
				}
			}
		}
	}
	
	protected static boolean buyItem(Player player, Chest chest, BuyableItem buyableItem, int sellAmount){
		Inventory playerInventory = chest.getInventory();

		ItemStack item = buyableItem.getItem(sellAmount);
		double cost = buyableItem.getSellPrice(sellAmount);
		
		String itemName = getFriendlyName(item);
		if (playerInventory.containsAtLeast(item, sellAmount)) {
			playerInventory.removeItem(item);
			Survival.getInstance().getEconomy().depositPlayer(player, cost);

			message(player, "&a", String.format("Sold %d x %s for %.2f", sellAmount, itemName, cost));
			return true;
		} else {
			message(player, "c", "You don't have enough " + itemName);
			return false;
		}
	}
	
	protected static boolean buyAll(Player player, Chest chest, BuyableItem buyableItem) {
		Inventory playerInventory = chest.getInventory();

		int sellAmount = 0;
		ItemStack item = buyableItem.getItem(sellAmount);

		// count whole stacks at a time
		int delta = 64;
		while(playerInventory.containsAtLeast(item, sellAmount+delta)) {
			sellAmount += delta;
		}
		
		// count any extra items 1 at a time
		delta = 1;
		while(playerInventory.containsAtLeast(item, sellAmount+delta)) {
			sellAmount += delta;
		}
		
		if(sellAmount == 0) {
			//message(player, "&c", "You don't have any " + getFriendlyName(item));
			return false;
		}
		
		return buyItem(player, chest, buyableItem, sellAmount);
	}
	
	private static String getFriendlyName(ItemStack item) {
		String badName;
		if (item.getItemMeta().hasDisplayName()) {
			badName = item.getItemMeta().getDisplayName();
		} else {
			badName = item.getType().toString();
		}
		
		StringBuilder sb = new StringBuilder();
		for (String str : badName.split("_"))
		sb.append(" ").append(Character.toUpperCase(str.charAt(0))).append(str.substring(1).toLowerCase());
		return sb.toString().trim().replace("Diode", "Redstone Repeater").replace("Thin Glass", "Glass Pane").replace("Wood ", "Wooden ");
	}
	
	
	public static void message(Player player, String colorCode, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', colorCode + message));
	}
	
	public ItemStack createWandItem(int uses) {
		ItemStack is = new ItemStack(Material.BLAZE_ROD);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Sell Wand " + ChatColor.GRAY + "Uses: " + uses);
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Right Click a Chest to use.");
		is.setItemMeta(im);
		return is;
	}
}
