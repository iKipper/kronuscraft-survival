package com.kronuscraftmc.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.kronuscraft.util.FileUtil;
import com.kronuscraftmc.shops.BuyableItem;
import com.kronuscraftmc.shops.ShopConfig;

public class SellChest implements Listener, CommandExecutor {
	public Map<UUID, Location> chests = new HashMap<UUID, Location>();
	private FileConfiguration config = Survival.getInstance().getConfig();
	
	public SellChest() {
		ConfigurationSection section = Survival.getInstance().getConfig().getConfigurationSection("SellChests");
		if (section != null) {
			for (String key : section.getKeys(false)) {
				ConfigurationSection location = section.getConfigurationSection(key + ".Location");
				Location loc = FileUtil.loadLocationFromSection(location);
				chests.put(UUID.fromString(key), loc);
			}
		}
	}
	
	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if(s.isOp()) {
			Player p = (Player) s;
			p.getInventory().addItem(createChestItem());
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fGiven you a &aSell Chest"));
		}
		return true;
	}


    @SuppressWarnings("rawtypes")
	@EventHandler
	public void onItemMove(InventoryMoveItemEvent event) {
		Iterator it = chests.entrySet().iterator();
	    while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			if(event.getDestination() instanceof Chest) {
				Chest chest = (Chest) event.getDestination();
				System.out.println(event.getDestination().getLocation());
				System.out.println(pair.getValue());
				Location loc = chest.getLocation();
				if(loc.getX() == ((Location)pair.getValue()).getX()) {
					if(loc.getY() == ((Location)pair.getValue()).getY()) {
						if(loc.getZ() == ((Location)pair.getValue()).getZ()) {
							UUID uuid = (UUID) pair.getKey();
							for (ShopConfig shops : Survival.getInstance().getShopManager().getShopConfigs()) {
								if (shops != null) {
									BuyableItem[] buyItems = shops.getItems();
									
									for (BuyableItem buyItem : buyItems) {
										SellWand.buyAll(Bukkit.getPlayer(uuid), chest, buyItem);
									}
								}
							}
						}
					}
				}
			}
			it.remove();
		}
	}
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
    	if(e.getItemInHand().getType() == Material.CHEST) {
    		if(e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Auto Sell Chest")) {
    			Location loc = e.getBlockPlaced().getLocation();
    			chests.put(e.getPlayer().getUniqueId(), loc.getBlock().getLocation());
    			String uuid = e.getPlayer().getUniqueId().toString();
    			config.set("SellChests." + uuid + ".Location.X", loc.getX());
    			config.set("SellChests." + uuid + ".Location.Y", loc.getY());
    			config.set("SellChests." + uuid + ".Location.Z", loc.getZ());
    			config.set("SellChests." + uuid + ".Location.W", loc.getWorld().getName());
    			Survival.getInstance().saveConfig();
    			e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&aAuto Sell Chest setup. &7(" + loc.getX() + " " + loc.getY() + " " + loc.getZ() + ")"));
    		}
    	}
    }
    
    @SuppressWarnings("rawtypes")
	@EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
    	if(e.getBlock() instanceof Chest) {
    		Chest chest = (Chest) e.getBlock();
			String uuid = e.getPlayer().getUniqueId().toString();
    		Iterator it = chests.entrySet().iterator();
    	    while (it.hasNext()) {
    			Map.Entry pair = (Map.Entry)it.next();
    			if(chest.getLocation().getBlockX() == ((Location) pair.getValue()).getBlockX()) {
        			if(chest.getLocation().getBlockY() == ((Location) pair.getValue()).getBlockY()) {
            			if(chest.getLocation().getBlockZ() == ((Location) pair.getValue()).getBlockZ()) {
            				e.getPlayer().getInventory().addItem(createChestItem());
            				e.getPlayer().sendMessage(ChatColor.DARK_RED + "Auto Sell Chest broken.");
            				chests.remove(pair.getValue());
            				config.set("SellChests." + uuid + ".Location.X", "");
            				config.set("SellChests." + uuid + ".Location.Y", "");
            				config.set("SellChests." + uuid + ".Location.Z", "");
            				config.set("SellChests." + uuid + ".Location.W", "");
            				config.set("SellChests." + uuid, "");
            				Survival.getInstance().saveConfig();
            			}
            		}
    			}
    	    }
    	    it.remove();
    	}
    }
    
	public ItemStack createChestItem() {
		ItemStack is = new ItemStack(Material.CHEST);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Auto Sell Chest");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Place the Chest to use.");
		is.setItemMeta(im);
		return is;
	}
}
