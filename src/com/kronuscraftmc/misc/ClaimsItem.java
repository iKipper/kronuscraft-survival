package com.kronuscraftmc.misc;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ClaimsItem implements Listener, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if(s.isOp()) {
			int money = Integer.valueOf(args[0]);
			Player p = (Player) s;
			p.getInventory().addItem(createClaimsItem(money));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fGiven Claim Token &a" + money + "&f!"));
		}
		return true;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getPlayer().getInventory().getItemInMainHand().getType() == Material.SUNFLOWER) {
				ItemStack is = e.getPlayer().getInventory().getItemInMainHand();
				String name = is.getItemMeta().getDisplayName();
				if (name.startsWith(ChatColor.RED + "Claims Token " + ChatColor.GRAY + "Claims: ")) {
					String splitted = name.replace(ChatColor.RED + "Claims Token " + ChatColor.GRAY + "Claims: ", "");
					int amount = Integer.parseInt(splitted);
					Inventory inv = e.getPlayer().getInventory();
					if (is.getAmount() > 1) {
						is.setAmount(is.getAmount() - 1);
					} else {
						inv.removeItem(is);
					}
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "acb " + e.getPlayer().getName() + " " + amount);
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fYou have received &a" + amount + "&f extra Claim Blocks!"));
				}
			}
		}
	}
	
	public ItemStack createClaimsItem(int uses) {
		ItemStack is = new ItemStack(Material.SUNFLOWER);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Claims Token " + ChatColor.GRAY + "Claims: " + uses);
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Right Click to use.");
		is.setItemMeta(im);
		return is;
	}
}
