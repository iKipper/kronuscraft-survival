package com.kronuscraftmc.player;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.database.SurvivalPlayerData;
import com.kronuscraftmc.kronuscraft.players.GamePlayer;

import me.confuser.barapi.BarAPI;

public class SurvivalPlayer {

	private Player player;
	private GamePlayer gPlayer;
	private Scoreboard board;
	private int bossCredits;
	public int scoreboardMoney, scoreboardRank, scoreboardCredits, scoreboardVotes, scoreboardBosses;
	private RankList rank;

	public SurvivalPlayer(UUID uuid, int survivalRank, int bossCredits) {
		this.player = Bukkit.getPlayer(uuid);
		this.gPlayer = KronusCraft.getInstance().getPlayerManager().getGamePlayer(uuid);
		this.rank = RankList.getPlayerRank(survivalRank);
		this.bossCredits = bossCredits;
		this.scoreboardMoney = (int) getMoney();
		this.scoreboardBosses = bossCredits;
		this.scoreboardRank = getGamePlayer().getRank().getId();
		this.scoreboardCredits = getGamePlayer().getVoteCredits();
		this.scoreboardVotes = Survival.getInstance().getVoteParty().getRemainingVotes();
		
		System.out.println(gPlayer.getPlayer().getName() + " " + rank);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				setupScoreboard();
				setupBossBars();				
			}
		}, 20L);
	}

	public static String getMoneyTag(double number) {
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		formatter.setDecimalFormatSymbols(symbols);	
		return formatter.format(number);
	}
	
	public Player getPlayer() {
		return player;
	}

	public GamePlayer getGamePlayer() {
		return gPlayer;
	}
	
	public int getBossCredits() {
		return bossCredits;
	}
	
	public void setBossCredits(int bossCredits) {
		this.bossCredits = bossCredits;
		SurvivalPlayerData.updateBossCredits(player.getUniqueId(), bossCredits);
	}
	
	public boolean checkCredits(int credits) {
		return scoreboardCredits == credits;
	}
	
	public int getScoreboardCredits() {
		return scoreboardCredits;
	}

	public void setScoreboardCredits(int scoreboardCredits) {
		this.scoreboardCredits = scoreboardCredits;
	}
	
	public boolean checkRank(int id) {
		return this.scoreboardRank == id;
	}
	
	public int getScoreboardRank() {
		return scoreboardRank;
	}

	public void setScoreboardRank(int rankid) {
		this.scoreboardRank = rankid;
	}

	public boolean checkMoney(int money) {
		return scoreboardMoney == money;
	}
	
	public int getScoreboardMoney() {
		return scoreboardMoney;
	}

	public void setScoreboardMoney(int money) {
		this.scoreboardMoney = money;
	}
	
	public boolean checkVotes(int votes) {
		return scoreboardVotes == votes;
	}
	
	public int getScoreboardVotes() {
		return scoreboardVotes;
	}
	
	public void setScoreboardVotes(int scoreboardVotes) {
		this.scoreboardVotes = scoreboardVotes;
	}
	
	public boolean chestBossCredits(int credits) {
		return scoreboardBosses == credits;
	}
	
	public int getScoreboardBosses() {
		return scoreboardBosses;
	}
	
	public void setScoreboardBosses(int scoreboardBosses) {
		this.scoreboardBosses = scoreboardBosses;
	}
	
	public RankList getRank() {
		return rank;
	}
	
	public void setRank(RankList rank) {
		SurvivalPlayerData.updateSurvivalRank(player.getUniqueId(), rank.getId());
		this.rank = rank;
	}

	public void removeMoney(double money) {
		Survival.getInstance().getEconomy().withdrawPlayer(player, money);
	}

	public double getMoney() {
		return Survival.getInstance().getEconomy().getBalance(player);
	}
	
	@SuppressWarnings("deprecation")
	public void setupBossBars() {
		BarAPI.removeBar(player);
		
		if(rank == RankList.KRONOS) {
			BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&eMAX RANK REACHED"));
			return;
		}
		RankList nextRank = RankList.getPlayerRank(rank.getId() + 1);
		double needed = nextRank.getCost() - Survival.getInstance().getEconomy().getBalance(player);
		if(needed > 0) {
		BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&fNext Rank:&e " +
		nextRank.getTag(false, false, nextRank.getChatColor()) + "&7(&a$" + getMoneyTag(needed) + "&7 for next Rank)"), 0F);
		} else {
			BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&eNEXT RANK AVALIALBE"));
		}
	}

	public void setupScoreboard() {	
		if (board != null) {
			if (player.getPlayer().getScoreboard().getObjective(DisplaySlot.SIDEBAR) == board.getObjective(DisplaySlot.SIDEBAR)) {
				player.getPlayer().getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
			}
		}
		
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		@SuppressWarnings("deprecation")
		Objective obj = board.registerNewObjective("new", "new");
		obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e--&8-&r&8[ &6&lKronusCraft&r &8]&m-&e--"));
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.getScore(ChatColor.RED + " ").setScore(13);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&6Player Info")).setScore(12);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Rank: " + getRank().getTag(false, false, getRank().getChatColor()))).setScore(11);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Kronus Credits: &r&e" + scoreboardCredits)).setScore(10);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Money: &r&e$" + getMoneyTag(scoreboardMoney))).setScore(9);
		obj.getScore(ChatColor.GREEN + " ").setScore(8);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&6Server Info")).setScore(7);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Online: &r&e" + Bukkit.getOnlinePlayers().size())).setScore(6);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Vote Party: &r&e" + Survival.getInstance().getVoteParty().getRemainingVotes() + "/30")).setScore(5);
		obj.getScore(ChatColor.GRAY + " ").setScore(4);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&6Information")).setScore(3);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Website: &e/website")).setScore(2);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&8�&7Store: &e/buy")).setScore(1);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&e------------------")).setScore(0);
		
		player.getPlayer().setScoreboard(board);
	}
}
