package com.kronuscraftmc.player;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.bukkit.ChatColor;

public enum RankList {
	TOURIST(1, "Tourist", ChatColor.DARK_GRAY, 0.0),
	EXPLORER(2, "Explorer", ChatColor.DARK_GRAY, 2500.0),
	SETTLER(3, "Settler", ChatColor.GRAY, 5000.0),	
	RESIDENT(4, "Resident", ChatColor.GRAY, 10000.0),
	TRADER(5, "Trader", ChatColor.WHITE, 25000.0),
	LANDLORD(6, "Landlord", ChatColor.WHITE, 50000.0),
	MAYOR(7, "Mayor", ChatColor.DARK_BLUE, 80000.0),
	REGNANT(8, "Regnant", ChatColor.BLUE, 150000.0),
	MONARCH(9, "Monarch", ChatColor.AQUA, 250000.0),
	TYCOON(10, "Tycoon", ChatColor.DARK_AQUA, 400000.0),
	LORD(11, "Lord", ChatColor.GREEN, 800000.0),
	EMPEROR(12, "Emperor", ChatColor.DARK_GREEN, 1500000.0),
	HERO(13, "Hero", ChatColor.DARK_GREEN, 2500000.0),
	LEGEND(14, "Legend", ChatColor.YELLOW, 5000000.0),
	CHAMPION(15, "Champion", ChatColor.GOLD, 7500000.0),
	IMMORTAL(16, "Immortal", ChatColor.RED, 10000000.0),
	GODLY(17, "Godly", ChatColor.DARK_RED, 15000000.0),
	TITAN(18, "Titan", ChatColor.BLACK, 20000000.0),
	KRONOS(19, "Kronos", ChatColor.WHITE, 30000000.0);
	
	private int id;
	private String tag;
	private ChatColor color;
	private double cost;

	private RankList(int id, String tag, ChatColor color, double cost) {
		this.id = id;
		this.tag = tag;
		this.color = color;
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public String getTag() {
		return tag;
	}

	public ChatColor getChatColor() {
		return color;
	}
	
	public double getCost() {
		return cost;
	}
	
	public String getCostTag() {
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		formatter.setDecimalFormatSymbols(symbols);	
		return formatter.format(cost);
	}
	
	public static RankList getPlayerRank(int i) {
		for (RankList ranks : RankList.values()) {
			if (i == ranks.getId()) {
				return ranks;
			}
		}
		return null;
	}
	
	public static RankList getPlayerRankByCost(double cost) {
		for (RankList ranks : RankList.values()) {
			if (cost == ranks.getCost()) {
				return ranks;
			}
		}
		return null;
	}
	
	public String getPrefix(boolean bold, boolean uppercase) {
		String name = tag;

		if (uppercase) {
			name = tag.toUpperCase();
		}
		if (bold) {
			name = color + "" + ChatColor.BOLD + name;
		}
		return name;
	}
	
	public String getTag(boolean bold, boolean uppercase, ChatColor text) {
		String name = text + tag;

		if (!tag.isEmpty()) {
			name = text + "[" + tag + "] " + text;
		}
		if (uppercase) {
			name = tag.toUpperCase();
		}
		if (bold) {
			name = color + "" + ChatColor.BOLD + name + text;
		}

		return name;
	}

	public String getTagPrefix(boolean bold, boolean uppercase, ChatColor text) {
		String name = text + tag;

		if (!tag.isEmpty()) {
			name = text + "[" + tag.charAt(0) + "] " + text;
		}
		if (uppercase) {
			name = tag.toUpperCase();
		}
		if (bold) {
			name = color + "" + ChatColor.BOLD + name + text;
		}

		return name;
	}
}
