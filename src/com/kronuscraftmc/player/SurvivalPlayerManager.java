package com.kronuscraftmc.player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.kronuscraftmc.kronuscraft.database.Database;

public class SurvivalPlayerManager {
	public static List<SurvivalPlayer> players = new ArrayList<SurvivalPlayer>();
	
	public static List<SurvivalPlayer> getPlayers() {
		return players;
	}
	
	public static void addPlayer(SurvivalPlayer player) {
		if(!players.contains(player)) {
			players.add(player);
		}
	}
	
	public static boolean removePlayer(SurvivalPlayer player) {
		return players.remove(player);
	}
	
	public static SurvivalPlayer getPlayer(UUID uuid) {
		for (SurvivalPlayer p : players) {
			if (p.getPlayer().getUniqueId().equals(uuid)) {
				return p;
			}
		}
		return null;
	}
	
	public static SurvivalPlayer getPlayer(String name) {
		for (SurvivalPlayer p : players) {
			if (p.getPlayer().getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
	
	public static SurvivalPlayer setupPlayer(UUID uuid) {
		try {
			String toRun = "SELECT survivalrank,bosscredits FROM users WHERE uuid = ?";
			PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return new SurvivalPlayer(uuid, rs.getInt(1), rs.getInt(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
