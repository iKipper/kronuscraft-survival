package com.kronuscraftmc.util;

import com.kronuscraftmc.kronuscraft.chat.Announcer;
import com.kronuscraftmc.kronuscraft.chat.Message;

public class KronusAnnouncer extends Announcer {
    
    public KronusAnnouncer(int space) {
        super(space);
        setupMessage();
    }
    
    private void setupMessage() {
        Message donate = new Message("&fInterested in helping development of the Network?", "&fUse &e&o/buy &fand browse our &aRanks&f,&a Commands &f& &aPerks&f!");
        Message vote = new Message("&fVote for us now, type &a&o/vote &fin chat for rewards!");
        Message help = new Message("&fNeed help? At any point use &a&o/help", "&fUseful information, and quick access available!", "&fUse &a&o/helpop <message> &fto talk to a member of staff");
        Message five = new Message("&fApply for &7[Member]&f rank now! Use &a&o/member&f in chat!");
        Message donate2 = new Message("&fSupport the development! Use &a&o/buy&f in chat,", "&fBrowse &cRanks&f, &cCosmetics &fand &cPerks&f!");
        Message auction = new Message("&fNeed a place to sell items not in the &a&o/shop&f?", "&fUse the &cAuction House&f! Type &a&o/ah &fin chat!");
        Message credits = new Message("&fHave spare &cKronus Credits&f? Use &a&o/credits&f in chat now!", "&fPurchase &cKits&f, &cCrate Keys&f, &cCommands &fand &cCurrency");
        Message jobs = new Message("&fJoin a vast variety of Jobs!", "&fUse &a&o/jobs browse&f in chat now!", "&fJobs offer extra &c$$&f and &cPerks&f!");
        Message kits = new Message("&fBrowse a list of kits available for use!", "&fType &a&o/kit&f in chat now!", "&fOr visit the &eKit Master&f at spawn!");
        addMessage(donate);
        addMessage(vote);
        addMessage(help);
        addMessage(five);
        addMessage(donate2);
        addMessage(auction);
        addMessage(credits);
        addMessage(jobs);
        addMessage(kits);
    }
}
