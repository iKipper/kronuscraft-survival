package com.kronuscraftmc.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayerJoinEventMessages {
    private List<String> messages;

    public PlayerJoinEventMessages(String playerName) {
        messages = new ArrayList<>(Arrays.asList(
                "",
                "",
                "",
                "",
                "&7--------------- &bKronusCraft Network &r&7---------------",
                "                     &7Welcome " + playerName + "&f!",
                "   &e&l  Earn Money &8--> &e&lRank Up &8--> &e&lExtra Perks",
                "         &cRank up &8| &cMcMMO &8| &cJobs &8| &cEconomy &8| &cShops",
                "    &cAuction &8| &cCrates &8| &cGrief Proof &8| &cCustom Developed",
                "",
                "  &7Voting earns &aMoney&7, &aVote Crate Keys &7and &aVote Credits&7!",
                "           &7Use command &a/vote&7 for voting links!",
                "   &oUse &a&o/buy&f&o to support the development of the Network!",
                "&7--------------- &bKronusCraft Network &r&7---------------"
        ));
    }

    public List<String> getMessages() {
        return messages;
    }
}