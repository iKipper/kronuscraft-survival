package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Mob {
	STRING("STRING", 3.0, 1.50),
	FEATHER("FEATHER", 2.0, 1.0),
	LEATHER("LEATHER", 5.0, 2.50),
	EGG("EGG", 1.0, 0.50),
	SLIME_BALL("SLIME_BALL", 75.0, 35.0),
	ARROW("ARROW", 5.0, 2.50),
	BLAZE_ROD("BLAZE_ROD", 10.0, 0.0),
	BONE("BONE", 4.0, 2.0),
	SPIDER_EYE("SPIDER_EYE", 4.0, 2.0),
	GUNPOWDER("GUNPOWDER", 5.0, 2.50),
	MAGMA_CREAM("MAGMA_CREAM", 10.0, 5.0),
	BLAZE_POWDER("BLAZE_POWDER", 10.0, 0.0),
	GHAST_TEAR("GHAST_TEAR", 25.0, 10.0),
	ROTTEN_FLESH("ROTTEN_FLESH", 3.0, 1.50),
	KELP("KELP", 10.0, 2.5),
	ENDER_EYE("ENDER_EYE", 100.0, 50.0),
	ENDER_PEARL("ENDER_PEARL", 25.0, 10.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Mob(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Mob item: Mob.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
