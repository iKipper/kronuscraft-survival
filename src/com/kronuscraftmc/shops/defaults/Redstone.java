package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Redstone {
	STICKY_PISTON("STICKY_PISTON", 75.0, 0.0),
	PISTON("PISTON", 65.0, 0.0),
	DISPENSER("DISPENSER", 100.00, 0.0), 
	DROPPER("DROPPER", 100.0, 0.0),
	HOPPER("HOPPER", 400.0, 0.0),
	TNT("TNT", 0, 10.0),
	STONE_PRESSURE_PLATE("STONE_PRESSURE_PLATE", 5.0, 0.0),
	OAK_PRESSURE_PLATE("OAK_PRESSURE_PLATE", 3.0, 0.0),
	REDSTONE_LAMP("REDSTONE_LAMP", 25.0, 0.0),
	REDSTONE_TORCH("REDSTONE_TORCH", 5.0, 0.0),
	TRIPWIRE_HOOK("TRIPWIRE_HOOK", 10.0, 0.0),
	LIGHT_WEIGHTED_PRESSURE_PLATE("LIGHT_WEIGHTED_PRESSURE_PLATE", 50.0, 0.0),
	HEAVY_WEIGHTED_PRESSURE_PLATE("HEAVY_WEIGHTED_PRESSURE_PLATE", 40.0, 0.0),
	DAYLIGHT_DETECTOR("DAYLIGHT_DETECTOR", 100.0, 0.0),
	REPEATER("REPEATER", 25.0, 0.0),
	COMPARATOR("COMPARATOR", 25.0, 0.0),
	STONE_BUTTON("STONE_BUTTON", 5.0, 0.0),
	TRAPPED_CHEST("TRAPPED_CHEST", 250.0, 0.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Redstone(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Redstone item: Redstone.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
