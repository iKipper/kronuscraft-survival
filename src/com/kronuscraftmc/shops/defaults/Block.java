package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Block {
	STONE("STONE", 3.0, 1.0),
	OAK_LOG("OAK_LOG", 4.0, 1.50),
	OAK_PLANKS("OAK_PLANKS", 1.0, 0.50),
	SPRUCE_LOG("SPRUCE_LOG", 4.0, 1.50),
	SPRUCE_PLANKS("SPRUCE_PLANKS", 1.0, 0.50),
	ACACIA_LOG("ACACIA_LOG", 4.0, 1.50),
	ACACIA_PLANKS("ACACIA_PLANKS", 1.0, 0.50),
	JUNGLE_LOG("JUNGLE_LOG", 4.0, 1.50),
	JUNGLE_PLANKS("JUNGLE_PLANKS", 1.0, 0.50),
	COBBLESTONE("COBBLESTONE", 2.0, 0.50),
	GRANITE("GRANITE", 1.0, 0.50),
	DIORITE("DIORITE", 1.0, 0.50),
	ANDESITE("ANDESITE", 1.0, 0.50),
	GRASS_BLOCK("GRASS_BLOCK", 2.0, 1.0),
	DIRT("DIRT", 1.0, 0.50),
	SAND("SAND", 2.50, 1.0),
	RED_SAND("RED_SAND", 2.50, 1.0),
	SANDSTONE("SANDSTONE", 5.00, 2.0),
	OBSIDIAN("OBSIDIAN", 50.0, 7.50),
	GLASS("GLASS", 4.0, 2.0),
	GRAVEL("GRAVEL", 1.0, 0.50),
	BRICKS("BRICKS", 5.0, 2.50),
	STONE_BRICKS("STONE_BRICKS", 3.0, 1.0),
	SEA_LANTERN("SEA_LANTERN", 10.0, 0.0),
	ICE("ICE", 3.0, 1.0),
	WHITE_WOOL("WHITE_WOOL", 4.0, 2.0),
	SPONGE("SPONGE", 250.0, 25.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Block(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Block item: Block.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
