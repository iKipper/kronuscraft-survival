package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Slimefun {

	IRONDUST("Iron Dust", 3),
	GOLDDUST("Gold Dust", 4),
	COPPERDUST("Copper Dust", 5),
	ALUMINUMDUST("Aluminum Dust", 4),
	
	TINDUST("Tin Dust", 6),
	ZINCDUST("Zinc Dust", 7),

	SILVERDUST("Silver Dust", 10),
	MAGNESIUM("Magnesium", 8),
	LEADDUST("Lead Dust", 10);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Slimefun(String name, int buyPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = 0;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Slimefun item: Slimefun.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
