package com.kronuscraftmc.shops;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.kronuscraftmc.inventorys.EnchantingInventory;
import com.kronuscraftmc.inventorys.SlimefunInventory;
import com.kronuscraftmc.inventorys.SpawnerInventory;
import com.kronuscraftmc.shops.defaults.Shop;

public class ShopManager {
	private final String LOG_HEADER = "[Shop Manager] ";
	
	private ShopConfig [] shopConfigs;
	private HashMap<String, BaseInventory> shopInventories;
	private HashMap<UUID, PlayerPreference> playerPreferences;

	public void setUpShops() {
		Shop[] shopList = Shop.values();
		shopConfigs = new ShopConfig[shopList.length];
		shopInventories = new HashMap<String, BaseInventory>();
		playerPreferences = new HashMap<UUID, PlayerPreference>();
		
		int i = 0;
		BaseInventory shopInv;
		for(Shop shop : shopList) {
			if(shop.name.equals("Enchanting")) {
				shopInv = new EnchantingInventory();
			} else if(shop.name.equals("Spawner")){
				shopInv = new SpawnerInventory();
			} else if(shop.name.equals("Slimefun")) {
				shopInv = new SlimefunInventory();
			}
			else {
				shopConfigs[i] = new ShopConfig(shop.name);
				shopInv = new ShopInventory(shop, shopConfigs[i]);
			}
			
			shopInventories.put(shop.name, shopInv);
			i++;
		}
		log("Shops successfully setup");
	} // end setUpShops()
	
	public void open(Player player, String shopName) {
		shopInventories.get(shopName).inventory.open(player);
	}
	
	private void log(String message) {
		System.out.println(LOG_HEADER + message);
	}
	
	public ShopConfig[] getShopConfigs() {return shopConfigs;}
	public HashMap<String, BaseInventory> getShops() {return shopInventories;}

	/**
	 * Gets the current buy amount for a player, initializes with 1 if they are new
	 * 
	 * @param player
	 * @return The player's buyAmount
	 */
	public int getBuyAmount(Player player) {
		UUID id = player.getUniqueId();
		if(!playerPreferences.containsKey(id)){
			trackPlayer(player);
		}
		return playerPreferences.get(id).buyAmount();
	}
	
	/**
	 * Changes the amount of items a player will buy with wrap around from 64 to 1
	 * 
	 * @param player The player who's buyAmount it being modified
	 * @param amount The amount to change buyAmount by
	 */
	public void changeBuyAmount(Player player, int amount) {
		UUID id = player.getUniqueId();
		if(!playerPreferences.containsKey(id)) {
			trackPlayer(player);
		}

		this.playerPreferences.get(id).changeBuyAmount(amount);
	}
	
	
	/**
	 * Sets if the player has sell all function turned on.
	 * 
	 * @param player The player who's sell all function it being modified
	 * @param on The new value for the player's sell all preference
	 */
	public void toggleSellAll(Player player) {
		UUID id = player.getUniqueId();
		PlayerPreference preference = playerPreferences.get(id); 
		preference.toggleSellAll();
	}
	
	
	/**
	 * Increments the amount of items a player will buy with wrap around from 64 to 1
	 * 
	 * @param player The player who's sellAll preference is being reset
	 */
	public void resetSellAllPreference(Player player) {
		UUID id = player.getUniqueId();
		
		if(!playerPreferences.containsKey(id)){
			trackPlayer(player);
		}
		
		PlayerPreference preference = playerPreferences.get(id); 
		if(preference.sellAll()) {
			preference.toggleSellAll();
		}
	}
	
	public boolean getSellAll(Player player) {
		UUID id = player.getUniqueId();
		if(!playerPreferences.containsKey(id)){
			trackPlayer(player);
		}
		return playerPreferences.get(id).sellAll();
	}
	
	/**
	 * Starts tracking a player's buyAmount and sellAll preferences
	 * 
	 * @param player The player to track
	 */
	private void trackPlayer(Player player) {
		UUID id = player.getUniqueId();
		this.playerPreferences.put(id, new PlayerPreference(1, false));
	}
	
	private class PlayerPreference{
		private int buyAmount;
		private boolean sellAll;
		public PlayerPreference(int buyAmount, boolean sellAll){
			this.buyAmount = buyAmount;
			this.sellAll = sellAll;
		}
			
		public int buyAmount() {
			return this.buyAmount;
		}
		
		public void changeBuyAmount(int amount) {
			this.buyAmount += amount;
			
			wrapBuyAmount();
		}
		
		public boolean sellAll() {
			return this.sellAll;
		}
		
		public void toggleSellAll() {
			this.sellAll = !sellAll;
		}
				
		private void wrapBuyAmount() {
			if(buyAmount > 64) {
				buyAmount %= 64;
			}
			else if(buyAmount < 1) {
				buyAmount += 64;
			}
		}
	}
}
