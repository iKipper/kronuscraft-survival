package com.kronuscraftmc.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.inventorys.credits.CreditMasterInventory;
import com.kronuscraftmc.player.SurvivalPlayerManager;

public class CreditsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command c, String l, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(SurvivalPlayerManager.getPlayer(p.getName()).getRank().getId() >= 3) {
				CreditMasterInventory.INVENTORY.open(((Player) sender));
			} else {     	
				p.sendMessage(ChatColor.RED + "You must be rank " + ChatColor.GRAY + "[Resident]" + ChatColor.RED + " to use this!");
			
			}
		}
		return true;
	}

}
