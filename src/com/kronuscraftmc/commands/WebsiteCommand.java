package com.kronuscraftmc.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class WebsiteCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command arg1, String arg2, String[] arg3) {
		s.sendMessage("");
		s.sendMessage("");
		s.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&owww.kronuscraftmc.com"));
		return false;
	}
}
