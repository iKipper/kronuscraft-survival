package com.kronuscraftmc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.bosses.master.BossMasterInventory;

public class BossCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command arg1, String arg2, String[] arg3) {
		BossMasterInventory.INVENTORY.open((Player) s);
		return true;
	}
}