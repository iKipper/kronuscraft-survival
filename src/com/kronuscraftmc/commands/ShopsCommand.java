package com.kronuscraftmc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.shops.ShopNav;

public class ShopsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command arg1, String arg2, String[] arg3) {
		ShopNav.INVENTORY.open((Player) s);
		return true;
	}
}