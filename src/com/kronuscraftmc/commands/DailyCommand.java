package com.kronuscraftmc.commands;

import com.kronuscraftmc.inventorys.DailyInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DailyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            DailyInventory.INVENTORY.open((Player) commandSender);
            return true;
        } else {
            commandSender.sendMessage("This is only accessable as player");
            return false;
        }
    }

}
