package com.kronuscraftmc.daily.storage;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class StorageReaderWriterManager {

    private StorageReader reader;
    private StorageWriter writer;

    private final String BASEPATH = "plugins/Survival/rewards";
    private final String LASTUSED = "lastused.txt";
    private final String STREAK = "streak.txt";

    private UUID targetUUID;

    public StorageReaderWriterManager() {
        writer = new StorageWriter();
        reader = new StorageReader();
    }

    public void setTargetUUID(UUID newTarget) {
        this.targetUUID = newTarget;
        createDirectoryIfNew();
    }

    private void createDirectoryIfNew() {
        File file = new File(String.format("%s/%s", BASEPATH, targetUUID.toString()));
        if (file.mkdirs()) {
            try {
                generateFile(generateStreakPath());
                generateFile(generateLastUsedPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateFile(String path) throws IOException {
        File file = new File(path);
        file.createNewFile();
        writer.setFilePath(path);
        writer.writeFile("0");
    }

    public long getLastUsedInMillis() {
        reader.setFilePath(generateLastUsedPath());
        String lastUsedString = reader.readFile();
        return Long.parseLong(lastUsedString);
    }

    public int getStreak() {
        reader.setFilePath(generateStreakPath());
        String streakString = reader.readFile();
        return Integer.parseInt(streakString);
    }

    public void updateCooldown() {
        writer.setFilePath(generateLastUsedPath());
        long currentTimeInMillis = System.currentTimeMillis();
        writer.writeFile(String.valueOf(currentTimeInMillis));
    }

    public void incrementStreak() {
        int streak = getStreak() + 1;
        writer.setFilePath(generateStreakPath());
        writer.writeFile(String.valueOf(streak));
    }

    private String generateLastUsedPath() {
        return String.format("%s/%s/%s", BASEPATH, targetUUID.toString(), LASTUSED);
    }

    private String generateStreakPath() {
        return String.format("%s/%s/%s", BASEPATH, targetUUID.toString(), STREAK);
    }
}
