package com.kronuscraftmc.daily.storage;

import java.io.*;

public class StorageWriter {

    private String filePath;
    private File file;

    public StorageWriter() {}

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        this.file = new File(filePath);
    }

    public void writeFile(String value) {
        checkIfFileExists();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(value);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkIfFileExists() {
        try {
           	file.mkdir();
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
