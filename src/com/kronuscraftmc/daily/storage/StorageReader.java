package com.kronuscraftmc.daily.storage;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class StorageReader {

    private String filePath;
    private File file;
    private Scanner scanner;

    public StorageReader() {}

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        file = new File(filePath);
    }

    public String readFile() {
        if (!checkIfFilePathPointsToFile()) {
            setupFile();
        }
        initScanner();
        return read();
    }

    private boolean checkIfFilePathPointsToFile() {
        return file.isFile();
    }

    private void setupFile() {
        try {
            boolean created = file.createNewFile();
            if (created) {
                initFileContents();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initFileContents() {
        StorageWriter writer = new StorageWriter();
        writer.setFilePath(filePath);
        writer.writeFile("0");
    }

    private void initScanner() {
        try {
            scanner = new Scanner(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String read() {
        return scanner.nextLine();
    }
}
