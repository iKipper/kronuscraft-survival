package com.kronuscraftmc.daily;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.inventorys.DailyInventory;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class DailyLoginListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        RewardsManager rewardsManager = new RewardsManager(new DefaultRewards());
        if (rewardsManager.isPlayerOffCooldown(e.getPlayer())) {
            Survival.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), () -> DailyInventory.INVENTORY.open(e.getPlayer()), 50L);
        }
    }
}
