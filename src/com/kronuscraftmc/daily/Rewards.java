package com.kronuscraftmc.daily;

import java.util.List;

import fr.minuskube.inv.ClickableItem;

public abstract class Rewards {

    abstract void initRewards();

    public abstract List<ClickableItem> getFirstSevenRewards();

    public abstract List<String> getCommands(int index);

}
