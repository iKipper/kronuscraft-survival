package com.kronuscraftmc.daily;

import com.kronuscraftmc.daily.storage.StorageReaderWriterManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class RewardsManager {

    StorageReaderWriterManager fileManager;

    private Rewards rewards;

    private final long millisInASecond = 1000;
    private final long millisInAMinute = 60 * millisInASecond;
    private final long millisInAnHour = 60 * millisInAMinute;
    private final long millisInADay = 24 * millisInAnHour;

    public RewardsManager(Rewards rewards) {
        this.rewards = rewards;
        fileManager = new StorageReaderWriterManager();
    }

    public void claimReward(Player p) {
        boolean playerOffCooldown = isPlayerOffCooldown(p);
        if (playerOffCooldown) {
            rewardPlayer(p);
        } else {
            messagePlayerCooldown(p);
        }
    }

    public boolean isPlayerOffCooldown(Player p) {
        long millisWaited = getMillisWaited(p);
        return millisWaited > millisInADay;
    }

    private long getMillisWaited(Player p) {
        fileManager.setTargetUUID(p.getUniqueId());
        long lastUsed = fileManager.getLastUsedInMillis();
        return System.currentTimeMillis() - lastUsed;
    }

    private void rewardPlayer(Player p) {
        fileManager.setTargetUUID(p.getUniqueId());
        int streak = fileManager.getStreak();
        fileManager.incrementStreak();
        fileManager.updateCooldown();
        List<String> commands = rewards.getCommands(streak % 7);
        executeCommands(commands, p);
        p.sendMessage(String.format(ChatColor.GREEN + "Current streak: %d", streak+1));
    }

    private void executeCommands(List<String> commands, Player p) {
        for (String command : commands) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), String.format(command, p.getName()));
        }
    }
    private void messagePlayerCooldown(Player p) {
        Long millisWaited = getMillisWaited(p);
        Long millisLeftToWait = millisInADay - millisWaited;
        int hoursLeft = (int) Math.floor(millisLeftToWait / millisInAnHour);
        int minutesLeft = (int) Math.floor(((millisLeftToWait - (hoursLeft * millisInAnHour)) / millisInAMinute));
        int secondsLeft = (int) Math.floor(((millisLeftToWait - ((hoursLeft * millisInAnHour) + (minutesLeft * millisInAMinute))) / millisInASecond));
        String message = String.format(ChatColor.RED + "Hours: %d - Minutes: %d - Seconds: %d left", hoursLeft, minutesLeft, secondsLeft);
        p.sendMessage(message);
    }

    public int getPlayerStreak(Player p) {
        fileManager.setTargetUUID(p.getUniqueId());
        return fileManager.getStreak();
    }

}
