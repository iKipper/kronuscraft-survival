package com.kronuscraftmc.daily;

import fr.minuskube.inv.ClickableItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DefaultRewards extends Rewards {

    List<ItemStack> items;
    List<ClickableItem> clickableItems;
    List<List<String>> rewardCommands;

    private RewardsManager rewardsManager;

    public DefaultRewards() {
        items = new ArrayList<>();
        clickableItems = new ArrayList<>();
        rewardsManager = new RewardsManager(this);
        initRewards();
    }

    @Override
    void initRewards() {
        initializeItemStacks();
        makeItemstacksClickable();
        initializeRewardCommands();
    }

    @Override
    public List<ClickableItem> getFirstSevenRewards() {
        int maxIndex = Math.min(clickableItems.size(), 7);
        return clickableItems.subList(0, maxIndex);
    }

    private void initializeItemStacks() {
        items.add(makeItemWithNameAndLore(Material.COAL_BLOCK,
                ChatColor.GRAY + "DAY 1",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 500$"));
        items.add(makeItemWithNameAndLore(Material.IRON_BLOCK,
                ChatColor.WHITE + "DAY 2",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Mystery key"));
        items.add(makeItemWithNameAndLore(Material.LAPIS_BLOCK,
                ChatColor.DARK_BLUE + "DAY 3",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Epic key"));
        items.add(makeItemWithNameAndLore(Material.REDSTONE_BLOCK,
                ChatColor.RED + "DAY 4",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Mystery key",
                ChatColor.WHITE + " + 50,000$"));
        items.add(makeItemWithNameAndLore(Material.GOLD_BLOCK,
                ChatColor.GOLD + "DAY 5",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Legendary key"));
        items.add(makeItemWithNameAndLore(Material.DIAMOND_BLOCK,
                ChatColor.BLUE + "DAY 6",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Mystery key",
                ChatColor.WHITE + " + 1 Epic key",
                ChatColor.WHITE + " + 1 Legendary key",
                ChatColor.WHITE + " + 100,000$"));
        items.add(makeItemWithNameAndLore(Material.EMERALD_BLOCK,
                ChatColor.GREEN + "DAY 7",
                ChatColor.GREEN + "REWARDS: ",
                ChatColor.WHITE + " + 1 Kronus key",
                ChatColor.WHITE + " + 1 Spawner key",
                ChatColor.WHITE + " + 200,000$"));
    }

    private ItemStack makeItemWithNameAndLore(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(Arrays.asList(lore));
        item.setItemMeta(itemMeta);
        return item;
    }

    private void initializeRewardCommands() {
        rewardCommands = new ArrayList<>();
        rewardCommands.add(generateListFromStrings("eco give %s 500"));
        rewardCommands.add(generateListFromStrings("crate key %s MYSTERY 1"));
        rewardCommands.add(generateListFromStrings("crate key %s EPIC 1"));
        rewardCommands.add(generateListFromStrings("crate key %s MYSTERY 1", "eco give %s 50000"));
        rewardCommands.add(generateListFromStrings("crate key %s LEGENDARY 1"));
        rewardCommands.add(generateListFromStrings("crate key %s MYSTERY 1", "crate key %s EPIC 1", "crate key %s LEGENDARY 1", "eco give %s 100000"));
        rewardCommands.add(generateListFromStrings("crate key %s KRONUS 1", "crate key %s SPAWNER 1", "eco give %s 200000"));
    }

    private List<String> generateListFromStrings(String... strings) {
        return Arrays.asList(strings);
    }

    private void makeItemstacksClickable() {
        for (int i = 0; i < items.size(); i++) {
            clickableItems.add(ClickableItem.of(items.get(i), this::executeClickAction));
        }
    }

    private void executeClickAction(InventoryClickEvent event) {
        rewardsManager.claimReward((Player) event.getWhoClicked());
    }

    public List<String> getCommands(int index) {
        int maxIndex = Math.min(rewardCommands.size(), index);
        return rewardCommands.get(maxIndex);
    }

    public RewardsManager getRewardsManager() {
        return this.rewardsManager;
    }
}
