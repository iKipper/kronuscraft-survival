package com.kronuscraftmc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.kronuscraftmc.bosses.Boss;
import com.kronuscraftmc.kronuscraft.chat.Announcer;
import com.kronuscraftmc.managers.CommandManager;
import com.kronuscraftmc.managers.EventManager;
import com.kronuscraftmc.managers.HandlerManager;
import com.kronuscraftmc.misc.VoteParty;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.kronuscraftmc.shops.ShopManager;
import com.kronuscraftmc.util.KronusAnnouncer;

import fr.minuskube.inv.InventoryManager;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import net.milkbowl.vault.economy.Economy;

public class Survival extends JavaPlugin {

	private static Survival instance;
	private HandlerManager handlerManager;
	public CommandManager commandManager;
	public EventManager eventManager;

	private VoteParty voteParty;
	private Economy economy;
	private InventoryManager invManager;
	private ShopManager shopManager;
	private GriefPrevention griefPrevention;
	private Boss bosses;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		instance = this;
		initializeManagers();
		voteParty = new VoteParty();
		checkEconomySetup();
		setupPlayers();
		setupAnnouncer();
		shopManager.setUpShops();
	}

	private void initializeManagers() {
		handlerManager = new HandlerManager(this);
		commandManager = new CommandManager(this, handlerManager);
		eventManager = new EventManager(this, handlerManager);
		invManager = new InventoryManager(this);
		shopManager = new ShopManager();
		griefPrevention = GriefPrevention.instance;
		bosses = new Boss();
	}

	private void checkEconomySetup() {
		if(!setupEconomy()) {
			Survival.log("Economy not set up!");
		}
	}
	
	private void setupAnnouncer() {
		Announcer announce = new KronusAnnouncer(720 * 10);
		announce.start();
	}

	public static Survival getInstance() {
		return instance;
	}

	private void setupPlayers() {
		for (Player players : Bukkit.getOnlinePlayers()) {
			SurvivalPlayer player = SurvivalPlayerManager.setupPlayer(players.getUniqueId());
			SurvivalPlayerManager.addPlayer(player);
		}
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}
		return (economy != null);
	}

	public Economy getEconomy() {
		return economy;
	}

	public InventoryManager getInvManager() {
		return invManager;
	}

	public ShopManager getShopManager() {
		return shopManager;
	}

	public GriefPrevention getGriefPrevention() {
		return griefPrevention;
	}

	public Boss getBosses() {
		return bosses;
	}

	public HandlerManager getHandlerManager() {
		return handlerManager;
	}

	public VoteParty getVoteParty() {
		return voteParty;
	}
	
	public static void log(String message) {
		System.out.println("[Survival] " + message);
	}
	
	public static void sendMessage(String message) {
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
	}
}