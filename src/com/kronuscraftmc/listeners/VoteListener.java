package com.kronuscraftmc.listeners;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.database.SurvivalPlayerData;
import com.kronuscraftmc.kronuscraft.players.GamePlayer;
import com.kronuscraftmc.kronuscraft.players.GamePlayerManager;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteListener implements Listener {

    @EventHandler
    public void onVote(VotifierEvent e) {
        String playerName = e.getVote().getUsername();
        incrementVoteParty();
        giveBonusRewards(playerName);
        giveMoneyRewards(playerName);
    }

    private void giveKeyRewards(String playerName) {
        String message = String.format("&7[&aKC&7]&b %s &7received &a1 Voter Key&7, &a1 Kronus Credit & &a$500&7!", playerName);
        String command = String.format("crate key %s voter", playerName);
        Survival.sendMessage(message);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
    }

    private void incrementVoteParty(){
        Survival.getInstance().getVoteParty().addVote();
    }

    private void giveBonusRewards(String playerName) {
        Random rand = new Random();
        int value = rand.nextInt(100);
        if (value < 3) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + playerName + " legendary");
            Survival.sendMessage("&7[&aKC&7]&b " + playerName + "&7 got lucky and gained &a1 Legendary Key&7!");
        } else if(value < 6) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + playerName + " epic");
            Survival.sendMessage("&7[&aKC&7]&b " + playerName + "&7 got lucky and gained &a1 Epic Key&7!");
        } else if(value < 10) {
            KronusCraft.getInstance().getPlayerManager().getGamePlayer(Bukkit.getPlayer(playerName).getUniqueId()).addVoteCredit(3);
            Survival.sendMessage("&7[&aKC&7]&b " + playerName + "&7 got lucky and gained &a3 Kronus Credits!");
        } else if(value < 15) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + playerName + " 10000");
            Survival.sendMessage("&7[&aKC&7]&b " + playerName + "&7 got lucky and gained &a$10,000&7!");
        }
    }

    private void giveMoneyRewards(String playerName) {
        if (doesPlayerExist(playerName)) {
            GamePlayer player = getGamePlayerByName(playerName);
            if(player != null) {
                rewardPlayer(player);
            }
        }
    }

    private boolean doesPlayerExist(String playerName) {
        return SurvivalPlayerData.checkPlayerExists(playerName);
    }

    @SuppressWarnings("deprecation")
    private GamePlayer getGamePlayerByName(String name) {
        GamePlayerManager gamePlayerManager = KronusCraft.getInstance().getPlayerManager();
        boolean isPlayerOnline = Bukkit.getOfflinePlayer(name).isOnline();
        UUID uuid = (isPlayerOnline) ? Bukkit.getPlayer(name).getUniqueId() : SurvivalPlayerData.getGamePlayerUUID(name);
        return gamePlayerManager.getGamePlayer(uuid);
    }

    private void rewardPlayer(GamePlayer player) {
        String playerName = player.getPlayer().getName();
        SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(playerName);
        if(player.getRank().getId() > 0) {
            payPlayerForVoting(playerName);
        }
        if(sPlayer.getRank().getId() >= 11) {
        	 payPlayerForVoting(playerName);
        } else if(sPlayer.getRank().getId() >= 15) {
       	 payPlayerForVoting(playerName);
       	 payPlayerForVoting(playerName);
        }
        payPlayerForVoting(playerName);
        giveKeyRewards(playerName);
        player.setVoteCredits(player.getVoteCredits() + 1);
    }

    private void payPlayerForVoting(String playerName) {
        String command = String.format("eco give %s 500", playerName);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
    }
}
