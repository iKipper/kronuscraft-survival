package com.kronuscraftmc.listeners;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.inventorys.WarpInventory;
import com.kronuscraftmc.inventorys.donate.DonateInventory;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.players.GamePlayer;
import com.kronuscraftmc.kronuscraft.players.GamePlayerManager;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class SurvivalListener implements Listener {
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		if(e.getEntityType() == EntityType.PHANTOM) {
			e.setCancelled(true);
		}
	}

	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent e) {
		Survival.getInstance().getShopManager().resetSellAllPreference(e.getPlayer());
		SurvivalPlayerManager.removePlayer(SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId()));
		
		for(SurvivalPlayer player : SurvivalPlayerManager.players) {
        	player.setupScoreboard();
        }
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		e.setDeathMessage(null);
		UUID uniquePlayerId = e.getEntity().getUniqueId();
		GamePlayerManager playerManager = KronusCraft.getInstance().getPlayerManager();
		GamePlayer gamePlayer = playerManager.getGamePlayer(uniquePlayerId);
		int rankId = gamePlayer.getRank().getId();
		if (rankId >= 14) {
			e.setKeepInventory(true);
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if(e.getPlayer().getWorld().getName().equals("world_nether")) {
			if(!e.getPlayer().isOp()) {
				if(e.getPlayer().getLocation().getY() > 127) {
					e.getPlayer().teleport(new Location(Bukkit.getWorld("spawn_world"), 40.5, 95, 19.5));
					e.getPlayer().sendMessage(ChatColor.RED + "This area is prohibited!");
				}
			}
		} else if(e.getPlayer().getWorld().getName().equals("spawn_world")) {
			if(e.getPlayer().getLocation().getY() < 10) {
				e.getPlayer().teleport(new Location(Bukkit.getWorld("spawn_world"), 40.5, 95, 19.5));
			}
		}
	}
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
		String message = constructMessage(e);
		KronusCraft.getInstance().requestMessage(e.getPlayer(), message);
	}

	private String constructMessage(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		GamePlayer gamePlayer = KronusCraft.getInstance().getPlayerManager().getGamePlayer(player.getUniqueId());
		SurvivalPlayer survivalPlayer = SurvivalPlayerManager.getPlayer(player.getName());
		String message = "";
		message += survivalPlayer.getRank().getTagPrefix(false, false, survivalPlayer.getRank().getChatColor());
		message += gamePlayer.getRank().getTag(false, false, gamePlayer.getRank().getChatColor());
		message += player.getDisplayName();
		message += ChatColor.WHITE + ": ";
		message += gamePlayer.getRank().getTextColor(gamePlayer);
		message += e.getMessage();
		return message;
	}

	@EventHandler
	public void onCommandEvent(PlayerCommandPreprocessEvent e) {
		if(e.getMessage().equalsIgnoreCase("/buy")) {
			DonateInventory.INVENTORY.open(e.getPlayer());
		} else if(e.getMessage().equalsIgnoreCase("/wild") || e.getMessage().equalsIgnoreCase("/rtp") || e.getMessage().equalsIgnoreCase("wilderness")) {
			e.setCancelled(true);
			WarpInventory.INVENTORY.open(e.getPlayer());
			if(e.getPlayer().getWorld().getName().equals("world_nether")) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.RED + "Command disabled in this world!");
			}
		} else if(e.getMessage().equalsIgnoreCase("/machines")) {
			e.getPlayer().performCommand("sf open_guide");
		} else if(e.getMessage().equalsIgnoreCase("/towny")) {
			e.getPlayer().performCommand("tgui main");
		} else if(e.getMessage().equalsIgnoreCase("t create") || e.getMessage().equalsIgnoreCase("t new") || e.getMessage().equalsIgnoreCase("town new") || e.getMessage().equalsIgnoreCase("town create")) {
			if(SurvivalPlayerManager.getPlayer(e.getPlayer().getName()).getRank().getId() < 7) {
				if(!e.getPlayer().isOp()) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.GREEN + "[Tycoon]" + ChatColor.RED + " to create a Town!");
				}
			}
		} else if(e.getMessage().equalsIgnoreCase("n new") || e.getMessage().equalsIgnoreCase("nation new")) {
			if(SurvivalPlayerManager.getPlayer(e.getPlayer().getName()).getRank().getId() < 8) {
				if(!e.getPlayer().isOp()) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.DARK_AQUA + "[Emperor]" + ChatColor.RED + " to create a Nation!");
				}
			}
		} else if(e.getMessage().equalsIgnoreCase("kit")) {
			if(SurvivalPlayerManager.getPlayer(e.getPlayer().getName()).getRank().getId() < 2) {
				if(!e.getPlayer().isOp()) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.GRAY + "[Resident]" + ChatColor.RED + " to use this!");
				}
			}
		}
	}

}
