package com.kronuscraftmc.listeners;

import com.kronuscraftmc.bosses.master.BossMasterInventory;
import com.kronuscraftmc.inventorys.HelpInventory;
import com.kronuscraftmc.inventorys.RankupInventory;
import com.kronuscraftmc.inventorys.WarpInventory;
import com.kronuscraftmc.inventorys.credits.CreditMasterInventory;
import com.kronuscraftmc.inventorys.donate.DonateInventory;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.kronuscraftmc.shops.ShopNav;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class NpcClickListener implements Listener {

    private Map<String, Consumer<NPCRightClickEvent>> npcActions;

    public NpcClickListener() {
        setupNpcActions();
    }

    private void setupNpcActions() {
        npcActions = new HashMap<>();
        npcActions.put(getTranslatedColorString("&eParkour &cLvl1"), event ->Bukkit.broadcastMessage(KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getTag(false, false, KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getChatColor()) + event.getClicker().getName() + ChatColor.WHITE + " has completed Parkour Level 1!"));
        npcActions.put(getTranslatedColorString("&eParkour &cLvl2"), event ->Bukkit.broadcastMessage(KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getTag(false, false, KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getChatColor()) + event.getClicker().getName() + ChatColor.WHITE + " has completed Parkour Level 2!"));
        npcActions.put(getTranslatedColorString("&eParkour &cLvl3"), event ->Bukkit.broadcastMessage(KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getTag(false, false, KronusCraft.getInstance().getPlayerManager().getGamePlayer(event.getClicker().getUniqueId()).getRank().getChatColor()) + event.getClicker().getName() + ChatColor.WHITE + " has completed Parkour Level 3!"));
        npcActions.put(getTranslatedColorString("&eWild William"), event -> WarpInventory.INVENTORY.open(event.getClicker()));
        npcActions.put(getTranslatedColorString("&eHelp Guide"), event -> HelpInventory.INVENTORY.open(event.getClicker()));
        npcActions.put(getTranslatedColorString("&eMiner Mack"), this::minerJackClicked);
        npcActions.put(getTranslatedColorString("&eShop"), event -> ShopNav.INVENTORY.open(event.getClicker()));
        npcActions.put(getTranslatedColorString("&eRank Master"), event -> RankupInventory.INVENTORY.open(event.getClicker()));
        npcActions.put(getTranslatedColorString("&eDonate Master"), this::donateMasterClicked);
        npcActions.put(getTranslatedColorString("&eKit Master"),this::kitMasterClicked);
        npcActions.put(getTranslatedColorString("&eCredit Master"), this::creditMasterClicked);
        npcActions.put(getTranslatedColorString("&eAuto Machines"), event -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "im openbook " + event.getClicker().getName()));
        npcActions.put(getTranslatedColorString("&eJob Master"), this::jobMasterClicked);
        npcActions.put(getTranslatedColorString("&eTown Master"), this::townMasterClicked);
        npcActions.put(getTranslatedColorString("&eSlimefun Guide"), event -> event.getClicker().performCommand("sf open_guide"));
        npcActions.put(getTranslatedColorString("&eBoss Master"), event -> BossMasterInventory.INVENTORY.open(event.getClicker()));
    }

    private void minerJackClicked(NPCRightClickEvent event) {
        Player p = event.getClicker();
        p.performCommand("mine");
        p.sendMessage(ChatColor.GRAY + "You're being teleported to the mine!...");
        p.sendMessage(ChatColor.GRAY + "Use command " + ChatColor.AQUA + "/mine" + ChatColor.GRAY + " at any point!");
    }

    private void donateMasterClicked(NPCRightClickEvent event) {
        Player p = event.getClicker();
        DonateInventory.INVENTORY.open(p);
        p.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
    }

    private void townMasterClicked(NPCRightClickEvent event) {
        event.getClicker().performCommand("tgui main");
        event.getClicker().sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Nations cost " + ChatColor.GREEN + "$250,000" + ChatColor.GRAY + ChatColor.ITALIC + " to create a Nation.");
    }
    
    private void kitMasterClicked(NPCRightClickEvent event) {
    	if(SurvivalPlayerManager.getPlayer(event.getClicker().getName()).getRank().getId() >= 2) {
    		event.getClicker().performCommand("kit");
    	} else {
    		event.getClicker().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.DARK_GRAY + "[Settler]" + ChatColor.RED + " to use this!");
    	}
    }
    
    private void creditMasterClicked(NPCRightClickEvent event) {
    	if(SurvivalPlayerManager.getPlayer(event.getClicker().getName()).getRank().getId() >= 4) {
    		CreditMasterInventory.INVENTORY.open(event.getClicker());
    	} else {
    		event.getClicker().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.GRAY + "[Resident]" + ChatColor.RED + " to use this!");
    	}
    }
    
    private void jobMasterClicked(NPCRightClickEvent event) {
    	if(SurvivalPlayerManager.getPlayer(event.getClicker().getName()).getRank().getId() >= 5) {
    		event.getClicker().performCommand("jobs browse");
    	} else {
    		event.getClicker().sendMessage(ChatColor.RED + "You must be rank " + ChatColor.WHITE + "[Landlord]" + ChatColor.RED + " to use this!");
    	}
    }

    @EventHandler
    public void onNPCRightClick(NPCRightClickEvent p) {
        String npcName = p.getNPC().getName();
        Consumer<NPCRightClickEvent> action = npcActions.get(npcName);
        if (action != null) {
            executeLambda(p, action);
        }
    }

    private void executeLambda(NPCRightClickEvent value, Consumer<NPCRightClickEvent> lambda) {
        lambda.accept(value);
    }

    private String getTranslatedColorString(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
