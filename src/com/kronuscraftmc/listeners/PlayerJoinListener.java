package com.kronuscraftmc.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.database.SurvivalPlayerData;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.kronuscraftmc.util.PlayerJoinEventMessages;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if(!p.hasPlayedBefore()) {
            newPlayerJoinActions(p);
        }
        scheduleJoinActions(p);
        
        for(SurvivalPlayer player : SurvivalPlayerManager.players) {
        	player.setupScoreboard();
        }
    }

    private void scheduleJoinActions(Player p) {
        playerJoinActions(p);
    }

    private void playerJoinActions(Player player) {
		KronusCraft.getInstance().getPlayerManager().addPlayer(SurvivalPlayerData.setupPlayer(player.getUniqueId(), player.getName()));
        SurvivalPlayer survivalPlayer = SurvivalPlayerManager.setupPlayer(player.getUniqueId());
        SurvivalPlayerManager.addPlayer(survivalPlayer);
        SurvivalPlayerData.updateName(player.getUniqueId());
        welcomePlayer(player);
    }

    private void newPlayerJoinActions(Player player) {
        Survival.getInstance().getEconomy().bankDeposit(player.getName(), 250.0);
        String command = String.format("crates key %s mystery", player.getName());
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				player.teleport(new Location(Bukkit.getWorld("spawn_world"), 40.5, 95, 19.5));
			}
		}, 15L);
        
    }

    private void welcomePlayer(Player player) {
        sendPlayerMessages(player);
        sendPlayerTitle(player);
    }

    private void sendPlayerMessages(Player player) {
        PlayerJoinEventMessages joinMessages = new PlayerJoinEventMessages(player.getName());
        for(String message : joinMessages.getMessages()) {
            String coloredMessage = ChatColor.translateAlternateColorCodes('&', message);
            player.sendMessage(coloredMessage);
        }
    }

    private void sendPlayerTitle(Player player) {
        String title = ChatColor.translateAlternateColorCodes('&', "&bSurvival");
        String subtitle = ChatColor.translateAlternateColorCodes('&', "&3Machines &r| &3Bosses&r | &3Custom Biomes");
        player.sendTitle(title, subtitle, 40, 40, 30);
    }

}
