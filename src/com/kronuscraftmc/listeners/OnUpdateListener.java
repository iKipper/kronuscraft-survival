package com.kronuscraftmc.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.kronuscraft.util.UpdateEvent;
import com.kronuscraftmc.kronuscraft.util.UpdateType;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

public class OnUpdateListener implements Listener {

    @EventHandler
    public void onUpdate(UpdateEvent e) {
        if (e.getType() == UpdateType.SLOW) {
            for (SurvivalPlayer player : SurvivalPlayerManager.getPlayers()) {
                checkPlayerCredits(player);
                checkPlayerRank(player);
                checkPlayerMoney(player);
                checkPlayerVotes(player);
                checkPlayerBossCredits(player);
            }
        } 
    }

    private void checkPlayerCredits(SurvivalPlayer player) {
        int credits = player.getGamePlayer().getVoteCredits();
        if (!player.checkCredits(credits)) {
            player.setScoreboardCredits(credits);
            player.setupScoreboard();
        }
    }

    private void checkPlayerRank(SurvivalPlayer player) {
        int rank = player.getRank().getId();
        if(!player.checkRank(rank)) {
            player.setScoreboardRank(rank);
        }
    }

    private void checkPlayerMoney(SurvivalPlayer player) {
        int money = (int) player.getMoney();
        if (!player.checkMoney(money)) {
            player.setScoreboardMoney(money);
            player.setupScoreboard();
            player.setupBossBars();
        }
    }

    private void checkPlayerVotes(SurvivalPlayer player) {
        int votes = Survival.getInstance().getVoteParty().getRemainingVotes();
        if (!player.checkVotes(votes)) {
            player.setScoreboardVotes(votes);
            player.setupScoreboard();
        }
    }

    private void checkPlayerBossCredits(SurvivalPlayer player) {
        int boss = player.getBossCredits();
        if (!player.chestBossCredits(boss)) {
            player.setScoreboardBosses(boss);
            player.setupScoreboard();
        }
    }
}
