package com.kronuscraftmc.bosses;

import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

public class BossListener implements Listener {

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if(e.getEntity().getKiller() != null) {
			if(e.getEntity().getKiller() instanceof Player) {
				Player damager = e.getEntity().getKiller();
				if(e.getEntity().getKiller().getWorld().getName().equals("Emathia") || e.getEntity().getKiller().getWorld().getName().equals("bossworld")) {
					if(e.getEntity().getCustomName() != null) {
						String name = e.getEntity().getCustomName();
						SurvivalPlayer player = SurvivalPlayerManager.getPlayer(damager.getUniqueId());
						if(!e.getEntity().getKiller().isFlying()) {
							for(BossMobs boss : BossMobs.values()) {
								if(name.equals(boss.mobName)) {
									Survival.getInstance().getBosses().bossDeath(boss, player);
								}
							}
						} else {
							damager.sendMessage(ChatColor.RED + "Fly is not allowed for Boss Fights!");
							damager.sendMessage(ChatColor.RED + "");
						}
					}
				}
			}
		}
		if(e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if(player.getWorld().getName().equals("bossworld")) {
				Survival.getInstance().getBosses().playerDeath(SurvivalPlayerManager.getPlayer(player.getUniqueId()));
			}
		}
	}
	
	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent e) {
		if (e.getDamager().getWorld().getName().equals("Emathia")	|| e.getDamager().getWorld().getName().equalsIgnoreCase("bossworld")) {
			if (e.getEntity().getCustomName() != null) {
				String name = e.getEntity().getCustomName();
				if (e.getDamager() instanceof Player) {
					Player p = (Player) e.getDamager();
					for (BossMobs boss : BossMobs.values()) {
						if (name.equals(boss.mobName)) {
							if (p.isFlying()) {
								p.setFlying(false);
								e.setCancelled(true);
								p.sendMessage(ChatColor.RED + "Fly is not allowed for Boss Fights!");
							} else {
								Survival.getInstance().getBosses().addAttackDamage(SurvivalPlayerManager.getPlayer(p.getUniqueId()), boss, e.getFinalDamage());
							}
						}
					}
				} else if (e.getDamager() instanceof Arrow) {
					Arrow arrow = (Arrow) e.getDamager();
					if (arrow.getShooter() != null) {
						if (arrow.getShooter() instanceof Player) {
							Player p = (Player) arrow.getShooter();
							for (BossMobs boss : BossMobs.values()) {
								if (name.equals(boss.mobName)) {
									if (p.isFlying()) {
										p.setFlying(false);
										p.sendMessage(ChatColor.RED + "Fly is not allowed for Boss Fights!");
										e.setCancelled(true);
									} else {
										Survival.getInstance().getBosses().addAttackDamage(SurvivalPlayerManager.getPlayer(p.getUniqueId()), boss, e.getFinalDamage());
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
