package com.kronuscraftmc.bosses.master;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.bosses.BossMobs;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class BossInfoInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("bosscurrency").provider(new BossInfoInventory()).size(6, 9).title("Boss Information").build();

	@Override
	public void init(Player player, InventoryContents cont) { 
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (BossMobs boss : BossMobs.values()) {
			List<String> lore = new ArrayList<String>();
			if(boss.mainBoss) {
				lore.add(ChatColor.translateAlternateColorCodes('&', "&e&oArena Boss"));
			} else {
				lore.add(ChatColor.translateAlternateColorCodes('&', "&e&oWorld Boss"));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			if(!boss.mainBoss) {
				lore.add(ChatColor.translateAlternateColorCodes('&', " &7Credits: &a" + boss.credits));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', " &7Health: &c" + boss.health));
			lore.add(ChatColor.translateAlternateColorCodes('&', " &7Damage: &c" + boss.damage));
			lore.add(ChatColor.translateAlternateColorCodes('&', " &7Movement Speed: &c" + boss.speed));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
			for(String lores : boss.lore) {
				lore.add(ChatColor.translateAlternateColorCodes('&', lores));
			}
			@SuppressWarnings("deprecation")
			ItemStack item = BossMobs.getHead(Bukkit.getOfflinePlayer(boss.mobHead), boss.mobName, lore);
			cont.set(boss.containerCoordinates[ROW], boss.containerCoordinates[COLUMN], ClickableItem.of(item, e -> player.closeInventory()));
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Boss Master GUI");
		cont.set(5, 4, ClickableItem.of(gui, e -> BossMasterInventory.INVENTORY.open(player)));
	}
	
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}