package com.kronuscraftmc.bosses.master;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class CurrencyExchangeInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("bosscurrency").provider(new CurrencyExchangeInventory()).size(3, 9).title("Currency Exchange").build();
	
	enum Shop {
		TEN (
				5,
				10000,
				Material.PAPER,
				new int[] {1,1}
				),
		TWENTY (
				10,
				20000,
				Material.PAPER,
				new int[] {1,3}
				),
		FIFTY (
				20,
				60000,
				Material.PAPER,
				new int[] {1,5} 
			),
		HUNDRED (
				50,
				250000,
				Material.PAPER,
				new int[] {1,7} 
				);
		
		public final int price;
		public final int credits;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(int price, int credits, Material guiObject, int[] containerCoordinates){
			this.price = price;
			this.credits = credits;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
	}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8&oExchange Boss Credits!"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7Price: &e" + shop.price + " Boss Credits"));
			ItemStack item = BaseInventory.createItem(shop.guiObject, ChatColor.translateAlternateColorCodes('&', "&a$" + SurvivalPlayer.getMoneyTag(shop.credits) + " In-Game Currency"), lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				if(exchangeBossCredits(SurvivalPlayerManager.getPlayer(player.getUniqueId()), shop.price, shop.credits)) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "You've exchanged &e" + shop.price + " Boss Credit(s) &ffor &a$" + shop.credits + "&f!"));
				} else {
					player.sendMessage(ChatColor.DARK_RED + "You need more Boss Credits!");
				}
				});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Boss Master GUI");
		cont.set(2, 4, ClickableItem.of(gui, e -> BossMasterInventory.INVENTORY.open(player)));
	}
	
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
	
	private boolean exchangeBossCredits(SurvivalPlayer p, int bossCredits, int currency) {
		if(p.getBossCredits() >= bossCredits) {
			p.setBossCredits(p.getBossCredits() - bossCredits);
			Survival.getInstance().getEconomy().depositPlayer(p.getPlayer(), currency);
		}
		return false;
	}
}