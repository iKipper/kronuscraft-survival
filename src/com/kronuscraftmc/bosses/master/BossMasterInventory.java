package com.kronuscraftmc.bosses.master;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class BossMasterInventory extends BaseInventory {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("bossmaster").provider(new BossMasterInventory()).size(5, 9).title("Boss Master").build();
	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fill(ClickableItem.empty(glass));
		
		ItemStack voteCredits = createItem(Material.NETHER_STAR, ChatColor.GREEN + "Vote Credits Exchange", createLore("&7", new String[] { "Exchange your Boss Credits", "for Vote Credits!" }));
		ClickableItem credits = ClickableItem.of(voteCredits, e -> {
			CreditsExchangeInventory.INVENTORY.open(player);
		});
		ItemStack currency = createItem(Material.PAPER, ChatColor.GREEN + "Currency Exchange", createLore("&7", new String[] { "Exchange your Boss Credits", "for in-game $$!" }));
		ClickableItem money = ClickableItem.of(currency, e -> {
			CurrencyExchangeInventory.INVENTORY.open(player);
		});
		ItemStack crateKeys = createItem(Material.TRIPWIRE_HOOK, ChatColor.GREEN + "Crate Keys Exchange", createLore("&7", new String[] { "Exchange your Boss Credits", "for Crate Keys!" }));
		ClickableItem keys  = ClickableItem.of(crateKeys, e -> {
			KeyExchangeInventory.INVENTORY.open(player);
		});
		ItemStack bossInfo = createItem(Material.SKELETON_SKULL, ChatColor.GREEN + "Boss Information", createLore("&7", new String[] { "Browse a list of Custom", "Bosses with their statistics!" }));
		ClickableItem boss  = ClickableItem.of(bossInfo, e -> {
			BossInfoInventory.INVENTORY.open(player);
		});
		
		cont.set(1, 2, credits);
		cont.set(1, 4, money);
		cont.set(1, 6, keys);
		cont.set(3, 4, boss);
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

}
