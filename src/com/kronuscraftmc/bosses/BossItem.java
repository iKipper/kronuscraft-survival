package com.kronuscraftmc.bosses;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

public class BossItem implements Listener, CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (s.isOp()) {
			String bossName = args[0];
			Player p = (Player) s;
			p.getInventory().addItem(createBossItem(bossName));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&bKC&7] &fGiven Boss Egg &a" + bossName + " &f!"));
			return true;
		}
		return false;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		SurvivalPlayer player = SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId());
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getPlayer().getInventory().getItemInMainHand().getType() == Material.DRAGON_EGG) {
				if (e.getPlayer().getWorld().getName().equals("bossworld")) {
					ItemStack is = e.getPlayer().getInventory().getItemInMainHand();
					if (is.getItemMeta().getDisplayName().startsWith(ChatColor.translateAlternateColorCodes('&', "Boss Egg &7- &6"))) {
						if(!Survival.getInstance().getBosses().bosses.contains(player)) {
							String splitted = is.getItemMeta().getDisplayName().replace(ChatColor.translateAlternateColorCodes('&', "Boss Egg &7- &6"), "");
							if (is.getAmount() > 1) {
								is.setAmount(is.getAmount() - 1);
							} else {
								e.getPlayer().getInventory().removeItem(is);
							}
							e.setCancelled(true);
							Survival.getInstance().getBosses().spawnBoss(splitted, player, e.getClickedBlock().getLocation().add(new Location(e.getClickedBlock().getWorld(), 0, 2, 0)));
						} else {
							e.getPlayer().sendMessage(ChatColor.RED + "You already have an active Boss Spawned!");
							e.setCancelled(true);
						}
					}
				} else {
					e.getPlayer().sendMessage(ChatColor.RED + "You must be in the Boss Arena!");
					e.setCancelled(true);
				}
			}
		}
	}

	private ItemStack createBossItem(String bossName) {
		ItemStack is = new ItemStack(Material.DRAGON_EGG);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fBoss Egg &7- &6" + bossName));
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Right Click to Spawn Boss");
		lore.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC + "Must be in Boss Arena (/arena)");
		im.setLore(lore);
		is.setItemMeta(im);
		return is;
	}
}