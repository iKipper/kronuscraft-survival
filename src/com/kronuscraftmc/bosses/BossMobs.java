package com.kronuscraftmc.bosses;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public enum BossMobs {
	SKELEKING (
			"�6The Skeleton King",
			new String[] { "&f&oSpawns Skeleton Minions when", "&f&oattacked! Strong, fast paced", "&f&oboss with medium attack damage!" },
			false,
			2,
			225.0,
			2,
			2.3,
			"Skeleton",
			new int[] {1,2}
			),
	SPIDERKING (
			"�6The Spider King",
			new String[] { "&f&oSpawns Spider Minions when", "&f&oattacked! Small, quick striking", "&f&oboss with medium poison damage!" },
			false,
			1,
			175.0,
			2.5,
			2.8,
			"Spider",
			new int[] {1,3}
			),
	ZOMBIEKING (
			"�6The Zombie King",
			new String[] { "&f&oStrong, slow armoured boss", "&f&owith medium to heavy attack damage!" },
			false,
			1,
			250.0,
			3.5,
			2.4,
			"Zombie",
			new int[] {1,4}
		),
	ORKKING (
			"�6Orc King",
			new String[] { "&f&oFast, strong and very", "&f&opowerful! Make sure to", "&f&ouse potions & strong weaponry!" },
			false,
			2,
			300.0,
			2.5,
			3.0,
			"Wither",
			new int[] {1,5}
			),
	UNDEADKING (
			"�6The Undead King",
			new String[] { "&f&oSlow but very strong!", "&f&oBe careful, you must not", "&f&oget close to his attacks!" },
			false,
			3,
			200.0,
			6,
			2.5,
			"Zombie",
			new int[] {1,6}
			),
	SLACKJAW (
			"�6Slackjaw",
			new String[] { "&f&oFast, armed Skeleton whom jockeys", "&f&oboss Jumper! Handles a", "&f&obow & magical spells!" },
			true,
			0,
			400.0,
			14,
			3.0,
			"Skeleton",
			new int[] {3,3}
			),
	JUMPER (
			"�6Jumper",
			new String[] { "&f&oRiden by Slackjaw,", "&f&oJumper is extremly fast and", "&f&oagile with medium attack damage!" },
			true,
			0,
			350.0,
			12,
			2.5,
			"Spider",
			new int[] {3,2}
			),
	THEAL (
			"�6Theal",
			new String[] { "&f&oSpawns in an army to", "&f&oassist! Theal uses Magical", "&f&oand brute strength to kill!" },
			true,
			0,
			600.0,
			13,
			3.2,
			"Enderdragon",
			new int[] {3,6}
			),
	OMEGA (
			"�6Omega",
			new String[] { "&f&oWill morph into a Giant", "&f&owhen health is low!", "&f&oVERY strong attack damage!" },
			true,
			0,
			800.0,
			15,
			2.8,
			"Herobrine",
			new int[] {3,5}
			);
	public final String mobName;
	public final String[] lore;
	public final boolean mainBoss;
	public final int credits;
	public final double health;
	public final double damage;
	public final double speed;
	public final String mobHead;
	public final int[] containerCoordinates;
	BossMobs(String mobName, String[] lore, boolean mainBoss, int credits, double health, double damage, double speed, String mobHead, int[] containerCoordinates){
		this.mobName = mobName;
		this.lore = lore;
		this.mainBoss = mainBoss;
		this.credits = credits;
		this.health = health;
		this.damage = damage;
		this.speed = speed;
		this.mobHead = "MHF_" + mobHead;
		this.containerCoordinates = containerCoordinates;
	}
	
	public static ItemStack getHead(OfflinePlayer player, String name, List<String> lores) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        skull.setLore(lores);
        skull.setOwningPlayer(player);
        item.setItemMeta(skull);
        return item;
    }
	
	public static BossMobs getBossByName(String name) {
		for (BossMobs mobs : BossMobs.values()) {
			if (name.equals(mobs.mobName.substring(2))) {
				return mobs;
			}
		}
		return null;
	}
}
