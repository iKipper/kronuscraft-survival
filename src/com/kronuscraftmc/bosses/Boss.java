package com.kronuscraftmc.bosses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.player.SurvivalPlayer;

public class Boss {

	public Map<SurvivalPlayer, Double> attackers = new HashMap<SurvivalPlayer, Double>();
	public List<SurvivalPlayer> bosses = new ArrayList<SurvivalPlayer>();
	
	public void spawnBoss(String bossName, SurvivalPlayer player, Location spawnBlock) {
		double x = spawnBlock.getX();
		double y = spawnBlock.getY();
		double z = spawnBlock.getZ();
		bosses.add(player);
		spawnBlock.getBlock().setType(Material.DRAGON_EGG);
		Survival.sendMessage("&7[&bKC&7]&c " + bossName + "&f is being spawned by " + player.getGamePlayer().getRank().getTag(false, false, player.getGamePlayer().getRank().getChatColor()) + player.getPlayer().getName());	
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			@Override
			public void run() {
				spawnBlock.add(new Location(spawnBlock.getWorld(), 0, -1, 0)).getBlock().setType(Material.AIR);
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mm mobs spawn " + bossName + " 1 " + spawnBlock.getWorld().getName() + "," + x + "," + y + "," + z);
				Survival.sendMessage("&7[&bKC&7]&c&l " + bossName + "&f&l has spawned in the Arena!");	
			}
		}, 20 * 10);
	}
	
	public void playerDeath(SurvivalPlayer player) {
		if(bosses.contains(player)) {
			bosses.remove(player);
		}
		if(attackers.containsKey(player)) {
			attackers.remove(player);
		}
	}
	
	public void bossDeath(BossMobs boss, SurvivalPlayer killer) {
		String name = killer.getGamePlayer().getRank().getTag(false, false, killer.getGamePlayer().getRank().getChatColor()) + killer.getPlayer().getName();
		Survival.sendMessage(boss.mobName + "&f has fallen! &7(" + name + "&7)");
		if(boss.mainBoss) {
			if(!attackers.isEmpty()) {
				double max = Collections.max(attackers.values());
				for(Map.Entry<SurvivalPlayer, Double> entry : attackers.entrySet()) {
					entry.getKey().getPlayer().playSound(entry.getKey().getPlayer().getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 10, 1);
					if(entry.getKey() != null) {
						if(entry.getValue() == max) {
							Survival.sendMessage("&7[&bKC&7] " + entry.getKey().getPlayer().getName() + "&f dealt the most damage! &7(" + max + ")");
							giveRewards(entry.getKey());
						} else {

						}
					} 
					if(entry.getKey() != null) {
						attackers.remove(entry.getKey());
					}
				}
			}
			Iterator<SurvivalPlayer> iter = bosses.iterator();
			while(iter.hasNext()) {
				SurvivalPlayer player = iter.next();
				giveRewards(player);
				iter.remove();
			}
		} else {
			killer.getPlayer().sendMessage(ChatColor.GREEN + "+ " + boss.credits + " Boss Credits");
			killer.setBossCredits(killer.getBossCredits() + boss.credits);
		}
	}
	
	public void addAttackDamage(SurvivalPlayer player, BossMobs boss, double damageDealt) {
		if (boss.mainBoss) {
			if (!bosses.contains(player)) {
				if (attackers.containsKey(player)) {
					double damage = attackers.get(player);
					attackers.remove(player);
					attackers.put(player, damage + damageDealt);
				} else {
					attackers.put(player, damageDealt);
				}
				System.out.println(player.getPlayer().getName() + " " + attackers.get(player));
			}
		}
	}
	
	public void giveRewards(SurvivalPlayer player) {
		Random rand = new Random();
		int value = rand.nextInt(100);
		String name = player.getPlayer().getName();
		String luck = "none";
		if(value < 6) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " kronus 1");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 100000");
			luck = "&aKronus Key &7& &a$100,000";
		} else if(value < 10) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " bosses 1");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 50000");
			luck = "&aBosses Key &7& &a$50,000";
		} else if(value < 15) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " legendary 2");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 25000");
			luck = "&aLegendary Key x2 &7& &a$25,000";
		} else if(value < 25) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 50000");
			luck = "&a$50,000";
		} else if(value < 50) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 25000");
			luck = "&a$25,000";
		} else {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " mystery 1");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 25000");
		}
		if(!luck.equals("none")) {
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&fYou got lucky and gained " + luck));
			player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);
		} else {
			player.getPlayer().sendMessage("You didn't strike lucky this time!");
		}
	}
}
