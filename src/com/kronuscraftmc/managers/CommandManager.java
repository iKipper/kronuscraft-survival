package com.kronuscraftmc.managers;

import org.bukkit.plugin.java.JavaPlugin;

import com.kronuscraftmc.bosses.BossItem;
import com.kronuscraftmc.commands.ArenaCommand;
import com.kronuscraftmc.commands.BossCommand;
import com.kronuscraftmc.commands.CreditsCommand;
import com.kronuscraftmc.commands.DailyCommand;
import com.kronuscraftmc.commands.HelpCommand;
import com.kronuscraftmc.commands.MineCommand;
import com.kronuscraftmc.commands.RanksCommand;
import com.kronuscraftmc.commands.ShopsCommand;
import com.kronuscraftmc.commands.WebsiteCommand;
import com.kronuscraftmc.misc.SellChest;
import com.kronuscraftmc.player.RankupCommand;

public class CommandManager {

    private JavaPlugin plugin;
    private HandlerManager handlerManager;

    public CommandManager(JavaPlugin plugin, HandlerManager handlerManager) {
        this.plugin = plugin;
        this.handlerManager = handlerManager;
        setupCommandExecutors();
    }

    private void setupCommandExecutors() {
        plugin.getCommand("help").setExecutor(new HelpCommand());
        plugin.getCommand("shops").setExecutor(new ShopsCommand());
        plugin.getCommand("shop").setExecutor(new ShopsCommand());
        plugin.getCommand("sell").setExecutor(new ShopsCommand());
        plugin.getCommand("rank").setExecutor(new RanksCommand());
        plugin.getCommand("boss").setExecutor(new BossCommand());
        plugin.getCommand("bossitem").setExecutor(new BossItem());
        plugin.getCommand("rankup").setExecutor(new RankupCommand());
        plugin.getCommand("ranks").setExecutor(new RankupCommand());
        plugin.getCommand("arena").setExecutor(new ArenaCommand());
        plugin.getCommand("mine").setExecutor(new MineCommand());
        plugin.getCommand("credits").setExecutor(new CreditsCommand());
        plugin.getCommand("moneyitem").setExecutor(handlerManager.getMoney());
        plugin.getCommand("creditsitem").setExecutor(handlerManager.getCredits());
        plugin.getCommand("claimsitem").setExecutor(handlerManager.getClaims());
        plugin.getCommand("sellwand").setExecutor(handlerManager.getWand());
        plugin.getCommand("daily").setExecutor(new DailyCommand());
        plugin.getCommand("website").setExecutor(new WebsiteCommand());
        plugin.getCommand("sellchest").setExecutor(new SellChest());
    }
}
