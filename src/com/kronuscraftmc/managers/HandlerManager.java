package com.kronuscraftmc.managers;

import com.kronuscraftmc.bosses.BossItem;
import com.kronuscraftmc.misc.*;
import org.bukkit.plugin.java.JavaPlugin;

public class HandlerManager {

    public JavaPlugin plugin;

    private MoneyItem money;
    private VoteCreditItem credits;
    private SellWand wand;
    private ClaimsItem claims;
    private BossItem boss;
    private SellChest chest;
    
    public HandlerManager(JavaPlugin plugin) {
        this.plugin = plugin;
        initializeHandlers();
    }

    private void initializeHandlers() {
        money = new MoneyItem();
        credits = new VoteCreditItem();
        wand = new SellWand();
        claims = new ClaimsItem();
        boss = new BossItem();
        chest = new SellChest();
    }

    public MoneyItem getMoney() {
        return money;
    }

    public VoteCreditItem getCredits() {
        return credits;
    }

    public SellWand getWand() {
        return wand;
    }

    public ClaimsItem getClaims() {
        return claims;
    }

    public BossItem getBoss() {
        return boss;
    }

    public SellChest getChest() {
		return chest;
	}

}
