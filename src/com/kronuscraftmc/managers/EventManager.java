package com.kronuscraftmc.managers;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.kronuscraftmc.bosses.BossListener;
import com.kronuscraftmc.daily.DailyLoginListener;
import com.kronuscraftmc.listeners.NpcClickListener;
import com.kronuscraftmc.listeners.OnUpdateListener;
import com.kronuscraftmc.listeners.PlayerJoinListener;
import com.kronuscraftmc.listeners.SurvivalListener;
import com.kronuscraftmc.listeners.VoteListener;

public class EventManager {

    private JavaPlugin plugin;
    private HandlerManager handlerManager;

    public EventManager(JavaPlugin plugin, HandlerManager handlerManager) {
        this.plugin = plugin;
        this.handlerManager = handlerManager;
        registerEvents();
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new SurvivalListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new BossListener(), plugin);
        Bukkit.getPluginManager().registerEvents(handlerManager.getMoney(), plugin);
        Bukkit.getPluginManager().registerEvents(handlerManager.getCredits(), plugin);
        Bukkit.getPluginManager().registerEvents(handlerManager.getWand(), plugin);
        Bukkit.getPluginManager().registerEvents(handlerManager.getClaims(), plugin);
        Bukkit.getPluginManager().registerEvents(handlerManager.getBoss(), plugin);
        Bukkit.getPluginManager().registerEvents(new NpcClickListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new OnUpdateListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new VoteListener(), plugin);
		Bukkit.getPluginManager().registerEvents(handlerManager.getChest(), plugin);
        Bukkit.getPluginManager().registerEvents(new DailyLoginListener(), plugin);
    }
}
