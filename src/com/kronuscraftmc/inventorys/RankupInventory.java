package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.player.RankList;
import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class RankupInventory  extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	enum Shop {
		EXPLORER (
				"Explorer",
				new String [] {"&f + 1 /sethome", "&f + /warp enchant", "&f + 250 Bonus Claim Blocks"},
				RankList.EXPLORER.getCost(),
				Material.CRAFTING_TABLE,
				new int[] {1,2},
				RankList.EXPLORER
			),
		SETTLER (
				"Settler",
				new String [] {"&f + Access to Auction House", "&f + Access to Kit Master", "&f + 250 Bonus Claim Blocks"},
				RankList.SETTLER.getCost(),
				Material.APPLE,
				new int[] {1,3},
				RankList.SETTLER
			),
		RESIDENT (
				"Resident",
				new String [] { "&f + Access to Credit Master", "&f + Access to Resident Kit",  "&f + 250 Bonus Claim Blocks"},
				RankList.RESIDENT.getCost(),
				Material.FLINT,
				new int[] {1,4},
				RankList.RESIDENT
		),
		TRADER (
				"Trader",
				new String [] { "&f + Access to Enchanting Shop", "&f + Silk Touch Spawners", "&f + 250 Bonus Claim Blocks"},
				RankList.TRADER.getCost(),
				Material.QUARTZ,
				new int[] {1,5},
				RankList.TRADER
		),
		LANDLORD (
				"Landlord",
				new String [] {"&f + 2 /sethome","&f + Access to Job Master",  "&f + 250 Bonus Claim Blocks" },
				RankList.LANDLORD.getCost(),
				Material.COAL,
				new int[] {1,6},
				RankList.LANDLORD
		),
		MAYOR (
				"Mayor",
				new String [] { "&f + Join upto 3 Jobs", "&f + Access to Spawner Shop", "&f + 250 Bonus Claim Blocks"},
				RankList.MAYOR.getCost(),
				Material.IRON_INGOT,
				new int[] {2,2},
				RankList.MAYOR
		),
		REGNANT (
				"Regnant",
				new String [] { "&f + 3 /sethome", "&f + Access to Regnant Kit", "&f + 250 Bonus Claim Blocks" },
				RankList.REGNANT.getCost(),
				Material.REDSTONE,
				new int[] {2,3},
				RankList.REGNANT
		),
		MONARCH (
				"Monarch",
				new String [] {  "&f + /pweather <sun/rain>", "&f + 4 items in Auction House", "&f + Access to Monarch Kit", "&f + 250 Bonus Claim Blocks" },
				RankList.MONARCH.getCost(),
				Material.LAPIS_LAZULI,
				new int[] {2,4},
				RankList.MONARCH
		),
		TYCOON (
				"Tycoon",
				new String [] {"&f + 4 /sethome", "&f + /ptime <day/night>", "&f + /stack", "&f + 500 Bonus Claim Blocks"},
				RankList.TYCOON.getCost(),
				Material.GOLD_INGOT,
				new int[] {2,5},
				RankList.TYCOON
		),
		LORD (
				"Lord",
				new String [] {"&f + Join upto 4 Jobs", "&f + 2x Voting $$", "&f + Access to Create Towns", "&f + 500 Bonus Claim Blocks"},
				RankList.LORD.getCost(),
				Material.DIAMOND,
				new int[] {2,6},
				RankList.LORD
		),
		EMPEROR (
				"Emperor",
				new String[] { "&f + Access to Create Nations", "&f + 5 items in Auction House", "&f + 500 Bonus Claim Blocks"},
				RankList.EMPEROR.getCost(),
				Material.EMERALD,
				new int[] {3,2},
				RankList.EMPEROR
		),
		HERO (
				"Hero",
				new String[] { "&f + 5 /sethome", "&f + Join upto 5 Jobs", "&f + Access to Hero Kit", "&f + 500 Bonus Claim Blocks"},
				RankList.HERO.getCost(),
				Material.OBSIDIAN,
				new int[] {3,3},
				RankList.HERO
		),
		LEGEND (
				"Legend",
				new String[] {"&f + /workbench", "&f + 6 items in Auction House", "&f + Access to Legend Kit", "&f + 500 Bonus Claim Blocks"},
				RankList.LEGEND.getCost(),
				Material.SLIME_BALL,
				new int[] {3,4},
				RankList.LEGEND
		),
		CHAMPION (
				"Champion",
				new String[] { "&f + /feed", "&f + Join upto 6 Jobs", "&f + 3x Voting $$", "&f + 500 Bonus Claim Blocks"},
				RankList.CHAMPION.getCost(),
				Material.BEDROCK,
				new int[] {3,5},
				RankList.CHAMPION
		),
		IMMORTAL (
				"Immortal",
				new String[] { "&f + /back", "&f + 4x Voting $$", "&f + 7 items in Auction House", "&f + Access to Immortal Kit", "&f + 500 Bonus Claim Blocks"},
				RankList.IMMORTAL.getCost(),
				Material.CREEPER_HEAD,
				new int[] {3,6},
				RankList.IMMORTAL
		),
		GODLY (
				"Godly",
				new String[] {"&f + 6 /sethome", "&f + 1 Small Player Vault (/pv)", "&f + Access to Godly Kit", "&f + 500 Bonus Claim Blocks"},
				RankList.GODLY.getCost(),
				Material.WITHER_SKELETON_SKULL,
				new int[] {4,3},
				RankList.GODLY
		),
		TITAN (
				"Titan",
				new String[] {"&f + 7 /sethome", "&f + Join upto 6 Jobs", "&f + 2 Small Player Vault (/pv)", "&f + 500 Bonus Claim Blocks"},
				RankList.TITAN.getCost(),
				Material.DRAGON_HEAD,
				new int[] {4,4},
				RankList.TITAN
		),
		KRONUS (
				"Kronus",
				new String[] {"&cPERKS COMING SOON"},
				RankList.KRONOS.getCost(),
				Material.END_CRYSTAL,
				new int[] {4,5},
				RankList.KRONOS
		);

		public final String name;
		public final String[] loreLines;
		public final double cost;
		public final Material guiObject;
		public final int[] containerCoordinates;
		public final RankList rank;
		Shop(String title, String[] loreLines, double cost, Material guiObject, int[] containerCoordinates, RankList rank){
			this.name = rank.getChatColor() + "" + rank.getTag(true, false, rank.getChatColor()) + "Rank";
			this.loreLines = loreLines;
			this.cost = cost;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
			this.rank = rank;
		}

	}
	
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("ranks").provider(new RankupInventory())
			.size(6, 9).title(ChatColor.GREEN + "Ranks GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fill(ClickableItem.empty(glass));
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getName());
		double money = sPlayer.getMoney();

		for(Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8&oCost: &b$" + shop.rank.getCostTag()));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7Perks:"));
			for(String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				rankupPlayer(sPlayer, shop.rank, money);
			});
			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Help GUI");
		cont.set(5, 4, ClickableItem.of(gui, e -> HelpInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
	
	}
}
