package com.kronuscraftmc.inventorys;

import com.kronuscraftmc.daily.DefaultRewards;
import com.kronuscraftmc.shops.BaseInventory;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class DailyInventory extends BaseInventory {

    public static final SmartInventory INVENTORY = SmartInventory.builder().id("dailyrewards").provider(new DailyInventory()).size(3, 9).title("Daily Rewards").build();

    private DefaultRewards rewards;

    @Override
    public void init(Player player, InventoryContents inventoryContents) {
        rewards = new DefaultRewards();
        fillInventoryWithGlass(inventoryContents);
        setupRewards(inventoryContents);
        displaySelectedReward(player, inventoryContents);
    }

    private void fillInventoryWithGlass(InventoryContents contents) {
        ItemStack placeHolder = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        placeHolder.setItemMeta(getItemNameMeta(placeHolder, " "));
        ClickableItem clickablePlaceHolder = ClickableItem.of(placeHolder, event -> {});
        contents.fillBorders(clickablePlaceHolder);
    }

    private void setupRewards(InventoryContents contents) {
        List<ClickableItem> items = rewards.getFirstSevenRewards();
        for (int i = 0; i < items.size(); i++) {
            contents.set(1, i+1, items.get(i));
        }
    }

    private void displaySelectedReward(Player p, InventoryContents contents) {
        if (rewards.getRewardsManager().isPlayerOffCooldown(p)) {
            setupReadyRewards(p, contents);
        } else {
            setupNotReadyRewards(p, contents);
        }
    }

    private void setupReadyRewards(Player p, InventoryContents contents) {
        ItemStack placeHolder = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        String name = ChatColor.GREEN + "" + ChatColor.BOLD + "Claim your daily reward!";
        placeHolder.setItemMeta(getItemNameMeta(placeHolder, name));
        ClickableItem clickablePlaceHolder = ClickableItem.of(placeHolder, event -> {});
        setSelector(p, contents, clickablePlaceHolder);
    }

    private void setupNotReadyRewards(Player p, InventoryContents contents) {
        ItemStack placeHolder = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        String name = ChatColor.RED + "" + ChatColor.BOLD + "Seems like you need to wait a bit longer!";
        placeHolder.setItemMeta(getItemNameMeta(placeHolder, name));
        ClickableItem clickablePlaceHolder = ClickableItem.of(placeHolder, event -> {});
        setSelector(p, contents, clickablePlaceHolder);
    }

    private ItemMeta getItemNameMeta(ItemStack item, String itemName) {
        ItemMeta placeHolderMeta = item.getItemMeta();
        placeHolderMeta.setDisplayName(itemName);
        return placeHolderMeta;
    }

    private void setSelector(Player p, InventoryContents contents, ClickableItem selector) {
        int streak = rewards.getRewardsManager().getPlayerStreak(p) % 7;
        contents.set(0, streak+1, selector);
        contents.set(2, streak+1, selector);
    }

    @Override
    public void update(Player player, InventoryContents inventoryContents) {
        setupNotReadyRewards(player, inventoryContents);
    }
}
