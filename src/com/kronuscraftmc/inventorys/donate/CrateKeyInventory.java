package com.kronuscraftmc.inventorys.donate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class CrateKeyInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("dcratekey").provider(new CrateKeyInventory())
			.size(4, 9).title(ChatColor.RED + "Crate Keys GUI").build();

	
	enum Shop {
		MYSTERY (
				"&a&l10 Mystery Crate Key",
				new String [] { "&f10x Mystery Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$1.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,1} 
				),
		EPIC (
				"&a&l10 Epic Crate Key",
				new String [] { "&f10x Epic Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$2.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,2}
				),
		LEGENDARY (
				"&a&l10 Legendary Crate Key",
				new String [] { "&f10x Legendary Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$7.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,3}
			),
		SLIIMEFUN (
				"&a&l10 Slimefun Crate Key",
				new String [] { "&f10x Slimefun Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$3.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,5}
				),
		SPAWNER (
				"&a&l5 Spawner Crate Key",
				new String [] { "&f5x Spawner Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$5.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,6}
				),
		KRONUS (
				"&a&l5 Kronus Crate Key",
				new String [] { "&f5x Kronus Crate Keys", "&fto be used at Spawn Crates!", "&7Price: &e$8.99&r"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,7}
				);
		
		public final String name;
		public final String[] loreLines;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(String title, String[] loreLines, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.loreLines = loreLines;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&b&oClick to view link!"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			for (String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = BaseInventory.createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "Click here: " + ChatColor.AQUA + "http://kronuscraft.buycraft.net/category/1296538");
				player.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
			});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Donate GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> DonateInventory.INVENTORY.open(player)));
	}
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
