package com.kronuscraftmc.inventorys.credits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class CommandInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("command").provider(new CommandInventory())
			.size(5, 9).title("Commands").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a500 Kronus Credit"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/fly"));
		ItemStack item0 = createItem(Material.PAPER, "&eFly Command", lore0);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a150 Kronus Credit"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/feed"));
		ItemStack item1 = createItem(Material.PAPER, "&eFeed Command", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a200 Kronus Credit"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/heal"));
		ItemStack item2 = createItem(Material.PAPER, "&eHeal Command", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a250 Kronus Credit"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/back"));
		ItemStack item3 = createItem(Material.PAPER, "&eBack Command", lore3);
		
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a350 Kronus Credit"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/enderchest"));
		ItemStack item4 = createItem(Material.PAPER, "&eEnderchest Command", lore4);
		
		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a400 Kronus Credit"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/fix"));
		ItemStack item5 = createItem(Material.PAPER, "&eFix Command", lore5);	
		
		List<String> lore6 = new ArrayList<String>();
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a75 Kronus Credit"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/ptime"));
		ItemStack item6 = createItem(Material.PAPER, "&ePlayer Time Command", lore6);
		
		List<String> lore7 = new ArrayList<String>();
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a200 Kronus Credit"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/nick <name>"));
		ItemStack item7 = createItem(Material.PAPER, "&eNickname Command", lore7);
		
		List<String> lore8 = new ArrayList<String>();
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a100 Kronus Credit"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/skull <name>"));
		ItemStack item8 = createItem(Material.PAPER, "&eSkull Command", lore8);
		
		List<String> lore9 = new ArrayList<String>();
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a350 Kronus Credit"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&7Usage: &e&o/enchant <name> <level>"));
		ItemStack item9 = createItem(Material.PAPER, "&eEnchant Command", lore9);
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Credit Master");
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			hasVoteCredits(sPlayer, 500, new String[] {"essentials.fly"});
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			hasVoteCredits(sPlayer, 150, new String[] {"essentials.feed"});
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			hasVoteCredits(sPlayer, 200, new String[] {"essentials.heal"});
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			hasVoteCredits(sPlayer, 250, new String[] {"essentials.back"});
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			hasVoteCredits(sPlayer, 350, new String[] {"essentials.enderchest"});
		});
		ClickableItem item5Click = ClickableItem.of(item5, e -> {
			hasVoteCredits(sPlayer, 400, new String[] {"essentials.repair"});
		});
		ClickableItem item6Click = ClickableItem.of(item6, e -> {
			hasVoteCredits(sPlayer, 75, new String[] {"essentials.ptime"});
		});
		ClickableItem item7Click = ClickableItem.of(item7, e -> {
			hasVoteCredits(sPlayer, 200, new String[] {"essentials.nick", "essentials.nick.color"});
		});
		ClickableItem item8Click = ClickableItem.of(item8, e -> {
			hasVoteCredits(sPlayer, 100, new String[] {"essentials.skull", "essentials.skull.others"});
		});
		ClickableItem item9Click = ClickableItem.of(item9, e -> {
			hasVoteCredits(sPlayer, 350, new String[] {"essentials.enchant"});
		});
		
		cont.set(1, 1, item0Click);
		cont.set(1, 2, item1Click);
		cont.set(1, 3, item2Click);
		cont.set(1, 4, item3Click);
		cont.set(1, 5, item4Click);
		cont.set(1, 6, item5Click);
		cont.set(1, 7, item6Click);
		cont.set(2, 3, item7Click);
		cont.set(2, 4, item8Click);
		cont.set(2, 5, item9Click);
		
		cont.set(3, 4, ClickableItem.of(gui, e -> CreditMasterInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

	private void hasVoteCredits(SurvivalPlayer player, int voteCredits, String[] command) {
		int playerCredits = player.getGamePlayer().getVoteCredits();
		if(playerCredits >= voteCredits) {
			for(String s : command) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + player.getPlayer().getName() + " add " + s);
			}
			player.getGamePlayer().setVoteCredits(playerCredits - voteCredits);
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You've exchanged &a" + voteCredits + " Kronus Credit(s)&8!"));
		} else {
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You need more &aKronus Credits &8for that!"));
		}
	}
	
	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack createItem(Material m, String name) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return item;
	}
}