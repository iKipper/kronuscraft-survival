package com.kronuscraftmc.inventorys.credits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class KitInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("kitsinv").provider(new KitInventory())
			.size(5, 9).title("Kits GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> lore0 = new ArrayList<>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Miner Kit &7(/kit miner)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a20 Kronus Credit"));
		ItemStack item0 = createItem(Material.ARMOR_STAND, "&aMiner Kit ", lore0);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Farmer Kit &7(/kit farmer)"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a10 Kronus Credit"));
		ItemStack item1 = createItem(Material.ARMOR_STAND, "&aFarmer Kit ", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Adventure Kit &7(/kit adventure)"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a10 Kronus Credit"));
		ItemStack item2 = createItem(Material.ARMOR_STAND, "&aAdventure Kit", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Engineer Kit &7(/kit engineer)"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a15 Kronus Credit"));
		ItemStack item3 = createItem(Material.ARMOR_STAND, "&aEngineer Kit", lore3);
		
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Enchanter Kit &7(/kit enchanter)"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a25 Kronus Credit"));
		ItemStack item4 = createItem(Material.ARMOR_STAND, "&aEnchanter Kit", lore4);
		
		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Cactus Kit &7(/kit cactus)"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a20 Kronus Credit"));
		ItemStack item5 = createItem(Material.ARMOR_STAND, "&aCactus Kit", lore5);
		
		List<String> lore6 = new ArrayList<String>();
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Sugar Cane Kit &7(/kit sugar)"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a20 Kronus Credit"));
		ItemStack item6 = createItem(Material.ARMOR_STAND, "&aSugar Cane Kit", lore6);
		
		List<String> lore7 = new ArrayList<String>();
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Brewer Kit &7(/kit brewer)"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a30 Kronus Credit"));
		ItemStack item7 = createItem(Material.ARMOR_STAND, "&aBrewer Cane Kit", lore7);
		
		List<String> lore8 = new ArrayList<String>();
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Slimefun Tools Kit &7(/kit slimefuntools)"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a50 Kronus Credit"));
		ItemStack item8 = createItem(Material.ARMOR_STAND, "&aSlimefun Tools Kit", lore8);
		
		List<String> lore9 = new ArrayList<String>();
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Slimefun Dust Kit &7(/kit slimefundust)"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a50 Kronus Credit"));
		ItemStack item9 = createItem(Material.ARMOR_STAND, "&aSlimefun Dust Kit", lore9);
		
		List<String> lore10 = new ArrayList<String>();
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ Slimefun Machines Kit &7(/kit slimefunmachines)"));
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a75 Kronus Credit"));
		ItemStack item10 = createItem(Material.ARMOR_STAND, "&aSlimefun Machines Kit", lore10);
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Credit Master");
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			hasVoteCredits(sPlayer, 20, "ultimatekits.kit.miner");
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			hasVoteCredits(sPlayer, 10, "ultimatekits.kit.farmer");
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			hasVoteCredits(sPlayer, 10, "ultimatekits.kit.adventure");
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			hasVoteCredits(sPlayer, 15, "ultimatekits.kit.engineer");
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			hasVoteCredits(sPlayer, 25, "ultimatekits.kit.enchanter");
		});
		ClickableItem item5Click = ClickableItem.of(item5, e -> {
			hasVoteCredits(sPlayer, 20, "ultimatekits.kit.cactus");
		});
		ClickableItem item6Click = ClickableItem.of(item6, e -> {
			hasVoteCredits(sPlayer, 20, "ultimatekits.kit.sugar");
		});
		ClickableItem item7Click = ClickableItem.of(item7, e -> {
			hasVoteCredits(sPlayer, 30, "ultimatekits.kit.brewer");
		});
		ClickableItem item8Click = ClickableItem.of(item8, e -> {
			hasVoteCredits(sPlayer, 50, "ultimatekits.kit.slimefuntools");
		});
		ClickableItem item9Click = ClickableItem.of(item9, e -> {
			hasVoteCredits(sPlayer, 50, "ultimatekits.kit.slimefundust");
		});
		ClickableItem item10Click = ClickableItem.of(item10, e -> {
			hasVoteCredits(sPlayer, 75, "ultimatekits.kit.slimefunmachines");
		});
		
		cont.set(1, 2, item0Click);
		cont.set(1, 3, item1Click);
		cont.set(1, 4, item2Click);
		cont.set(1, 5, item3Click);
		cont.set(1, 6, item4Click);
		cont.set(2, 2, item5Click);
		cont.set(2, 3, item6Click);
		cont.set(2, 4, item7Click);
		cont.set(2, 5, item8Click);
		cont.set(2, 6, item9Click);
		cont.set(3, 4, item10Click);
		cont.set(3, 7, ClickableItem.of(gui, e -> CreditMasterInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

	private void hasVoteCredits(SurvivalPlayer player, int voteCredits, String permission) {
		int playerCredits = player.getGamePlayer().getVoteCredits();
		if(playerCredits >= voteCredits) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + player.getPlayer().getName() + " add " + permission);
			player.getGamePlayer().setVoteCredits(playerCredits - voteCredits);
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You've exchanged &a" + voteCredits + " Kronus Credit(s)"));
		} else {
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You need more &aKronus Credits &8for that!"));
		}
	}
	
	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack createItem(Material m, String name) {
		return createItem(m, name, new ArrayList<>());
	}
}