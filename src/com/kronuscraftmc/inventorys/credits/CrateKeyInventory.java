package com.kronuscraftmc.inventorys.credits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.player.SurvivalPlayer;
import com.kronuscraftmc.player.SurvivalPlayerManager;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class CrateKeyInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("cratekey").provider(new CrateKeyInventory())
			.size(4, 9).title("Crate Keys").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Mystery Crate Key"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a5 Kronus Credit"));
		ItemStack item0 = createItem(Material.TRIPWIRE_HOOK, "&aMystery Crate Key ", lore0);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Epic Crate Key"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a10 Kronus Credit"));
		ItemStack item1 = createItem(Material.TRIPWIRE_HOOK, "&aEpic Crate Key ", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Legendary Crate Key"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a20 Kronus Credit"));
		ItemStack item2 = createItem(Material.TRIPWIRE_HOOK, "&aLegendary Crate Key", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Slimefun Crate Key"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a5 Kronus Credit"));
		ItemStack item3 = createItem(Material.TRIPWIRE_HOOK, "&aSlimefun Crate Key", lore3);

		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Spawner Crate Key"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a20 Kronus Credit"));
		ItemStack item4 = createItem(Material.TRIPWIRE_HOOK, "&aSpawner Crate Key", lore4);
		
		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&e&o+ 1 Kronus Crate Key"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fKronus Credits: &a25 Kronus Credit"));
		ItemStack item5 = createItem(Material.TRIPWIRE_HOOK, "&aKronus Crate Key", lore5);
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Credit Master");
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			hasVoteCredits(sPlayer, 5, "mystery");
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			hasVoteCredits(sPlayer, 10, "epic");
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			hasVoteCredits(sPlayer, 20, "legendary");
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			hasVoteCredits(sPlayer, 5, "slimefun");
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			hasVoteCredits(sPlayer, 20, "spawner");
		});
		ClickableItem item5Click = ClickableItem.of(item5, e -> {
			hasVoteCredits(sPlayer, 25, "kronus");
		});
		
		cont.set(1, 1, item0Click);
		cont.set(1, 2, item1Click);
		cont.set(1, 3, item2Click);
		cont.set(1, 5, item3Click);
		cont.set(1, 6, item4Click);
		cont.set(1, 7, item5Click);
		cont.set(2, 4, ClickableItem.of(gui, e -> CreditMasterInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

	private void hasVoteCredits(SurvivalPlayer player, int voteCredits, String crate) {
		int playerCredits = player.getGamePlayer().getVoteCredits();
		if(playerCredits >= voteCredits) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " " + crate);
			player.getGamePlayer().setVoteCredits(playerCredits - voteCredits);
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You've exchanged &a" + voteCredits + " Kronus Credit(s)"));
		} else {
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You need more &aKronus Credits &8for that!"));
		}
	}
	
	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack createItem(Material m, String name) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return item;
	}
}