package com.kronuscraftmc.inventorys;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;
import com.kronuscraftmc.shops.BuyableItem;
import com.kronuscraftmc.shops.ShopNav;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class SlimefunInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	enum Shop {
		IRONDUST (
				"&6Iron Dust",
				Material.GUNPOWDER, 
				64,
				3,
				new int[] {1,1}
		),
		ALUMINUMDUST (
				"&6Aluminum Dust",
				Material.SUGAR, 
				64,
				4,
				new int[] {1,2}
		),
		GOLDUST (
				"&6Gold Dust",
				Material.GLOWSTONE_DUST, 
				64,
				5,
				new int[] {1,3}
		),
		COPPERDUST (
				"&6Copper Dust",
				Material.GLOWSTONE_DUST, 
				64,
				8,
				new int[] {1,4}
		),
		TINDUST (
				"&6Tin Dust",
				Material.SUGAR, 
				64,
				7,
				new int[] {1,5}
		),
		ZINCDUST (
				"&6Zinc Dust",
				Material.SUGAR, 
				64,
				10,
				new int[] {1,6}
		),
		
		MAGNESIUM (
				"&6Magnesium",
				Material.SUGAR, 
				64,
				12,
				new int[] {1,7}
		),
		LEADDUST (
				"&6Lead Dust",
				Material.GUNPOWDER, 
				64,
				20,
				new int[] {2,1}
		),
		SILVERDUST (
				"&6Silver Dust",
				Material.SUGAR, 
				64,
				25,
				new int[] {2,2}
		);

		public final String name;
		public final Material material;
		public final int amount;
		public final double sellPrice;
		public final int[] containerCoordinates;

		Shop(String title, Material material, int amount, int sellPrice, int[] containerCoordinates){
			this.name = title;
			this.material = material;
			this.amount = amount;
			this.sellPrice = sellPrice;
			this.containerCoordinates = containerCoordinates;
		}

	}
	
	public SlimefunInventory() {
		super.inventory = SmartInventory.builder().id("slimefun").provider(this)
				.size(4, 9).title(ChatColor.AQUA + "Slimefun Shop").build();
		super.navCoordinates = new int[] {2,4};
		super.icon = createItem(Material.PISTON, SHOP_NAME_COLOR + "Slimefun Shop", createLore(SHOP_DESCRIPTION_COLOR, NAV_HEADING,new String[] {"Slimefun items sell shop!","Dusts, Ingots, Tools and more!"}));
	}
	
	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fill(ClickableItem.empty(glass));
		
		for(Shop shop : Shop.values()) {
			BuyableItem buyItem = new BuyableItem(shop.name, shop.material, 0, shop.sellPrice);
			ItemStack item = buyItem.getIconWithName(shop.amount);
			ClickableItem icon = ClickableItem.of(item, e -> {
				buyItemSlimefun(player, buyItem, shop.amount, createItem(shop.material, shop.name));
			});
			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Shop GUI");
		cont.set(4, 4, ClickableItem.of(gui, e -> ShopNav.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
