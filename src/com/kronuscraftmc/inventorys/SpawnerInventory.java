package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.kronuscraftmc.shops.BaseInventory;
import com.kronuscraftmc.shops.ShopNav;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class SpawnerInventory extends BaseInventory {
	public SpawnerInventory() {
		super.inventory = SmartInventory.builder().id("spawner").provider(this)
				.size(6, 9).title(ChatColor.AQUA + "Spawner Shop").build();
		super.navCoordinates = new int[] {1,7};
		super.icon = createItem(Material.SPAWNER, SHOP_NAME_COLOR + "Spawner Shop", createLore(SHOP_DESCRIPTION_COLOR, NAV_HEADING,new String[] {"Spawners available here!","Zombies, Enderman, Skeleton and more!"}));
	}

	@Override
	public void init(Player player, InventoryContents cont) {
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$125,000"));
		ItemStack item0 = createItem(EntityType.CREEPER, "&aCreeper Spawner", lore0);

		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$70,000"));
		ItemStack item1 = createItem(EntityType.SKELETON, "&aSkeleton Spawner", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$75,000"));
		ItemStack item2 = createItem(EntityType.ZOMBIE, "&aZombie Spawner", lore2);		
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$50,000"));
		ItemStack item3 = createItem(EntityType.SPIDER, "&aSpider Spawner", lore3);

		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$50,000"));
		ItemStack item4 = createItem(EntityType.CAVE_SPIDER, "&aCave Spider Spawner", lore4);

		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$200,000"));
		ItemStack item5 = createItem(EntityType.BLAZE, "&aBlaze Spawner", lore5);

		List<String> lore6 = new ArrayList<String>();
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$125,000"));
		ItemStack item6 = createItem(EntityType.PIG_ZOMBIE, "&aPig Zombie Spawner", lore6);
		
		List<String> loreend = new ArrayList<String>();
		loreend.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500,000"));
		ItemStack itemend = createItem(EntityType.ENDERMAN, "&aEnderman Spawner", loreend);
		
		List<String> loreswitch = new ArrayList<String>();
		loreswitch.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$75,000"));
		ItemStack itemswitch= createItem(EntityType.WITCH, "&aWitch Spawner", loreswitch);
		
		List<String> loreslime = new ArrayList<String>();
		loreslime.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$1,000,000"));
		ItemStack itemslime = createItem(EntityType.SLIME, "&aSlime Spawner", loreslime);
		
		List<String> loregolem = new ArrayList<String>();
		loregolem.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$3,000,000"));
		ItemStack itemgolem = createItem(EntityType.IRON_GOLEM, "&aIron Golem Spawner", loregolem);
		
		
		List<String> lore9 = new ArrayList<String>();
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$15,000"));
		ItemStack item9 = createItem(EntityType.PIG, "&aPig Spawner", lore9);
		
		List<String> lore10 = new ArrayList<String>();
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$25,000"));
		ItemStack item10 = createItem(EntityType.COW, "&aCow Spawner", lore10);
		
		List<String> lore11 = new ArrayList<String>();
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$20,000"));
		ItemStack item11 = createItem(EntityType.SHEEP, "&aSheep Spawner", lore11);
		
		List<String> lore12 = new ArrayList<String>();
		lore12.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$15,000"));
		ItemStack item12 = createItem(EntityType.CHICKEN, "&aChicken Spawner", lore12);
		
		List<String> lore13 = new ArrayList<String>();
		lore13.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$25,000"));
		ItemStack item13 = createItem(EntityType.CHICKEN, "&aMushroom Cow Spawner", lore13);
		
		List<String> lore14 = new ArrayList<String>();
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$50,000"));
		ItemStack item14 = createItem(EntityType.CHICKEN, "&aHorse Spawner", lore14);
		
		List<String> lore15 = new ArrayList<String>();
		lore15.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500,000"));
		ItemStack item15 = createItem(EntityType.MAGMA_CUBE, "&aMagma Cube Spawner", lore15);
		
		
		
		List<String> lore16 = new ArrayList<String>();
		lore16.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$500"));
		ItemStack item16 = createItem(Material.VILLAGER_SPAWN_EGG, "&aVillager Egg", lore16);
		
		List<String> lore17 = new ArrayList<String>();
		lore17.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$500"));
		ItemStack item17 = createItem(Material.PIG_SPAWN_EGG, "&aPig Egg", lore17);
		
		List<String> lore18 = new ArrayList<String>();
		lore18.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$400"));
		ItemStack item18 = createItem(Material.SHEEP_SPAWN_EGG, "&aSheep Egg", lore18);
		
		List<String> lore19 = new ArrayList<String>();
		lore19.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$400"));
		ItemStack item19 = createItem(Material.COW_SPAWN_EGG, "&aCow Egg", lore19);
		
		List<String> lore20 = new ArrayList<String>();
		lore20.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$400"));
		ItemStack item20 = createItem(Material.CHICKEN_SPAWN_EGG, "&aChicken Egg", lore20);
		
		List<String> lore21 = new ArrayList<String>();
		lore21.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$250"));
		ItemStack item21 = createItem(Material.TROPICAL_FISH_SPAWN_EGG, "&aTropical Fish Egg", lore21);
		
		List<String> lore22 = new ArrayList<String>();
		lore22.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$200"));
		ItemStack item22 = createItem(Material.RABBIT_SPAWN_EGG, "&aRabbit Egg", lore22);
		
		List<String> lore23 = new ArrayList<String>();
		lore23.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$1,000"));
		ItemStack item23 = createItem(Material.HORSE_SPAWN_EGG, "&aHorse Egg", lore23);	
		
		List<String> lore24 = new ArrayList<String>();
		lore24.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$200"));
		ItemStack item24 = createItem(Material.CAT_SPAWN_EGG, "&aCat Egg", lore24);	
		
		List<String> lore25 = new ArrayList<String>();
		lore25.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$500"));
		ItemStack item25 = createItem(Material.PANDA_SPAWN_EGG, "&aPanda Egg", lore24);	
		
		List<String> lore26 = new ArrayList<String>();
		lore26.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$500"));
		ItemStack item26 = createItem(Material.POLAR_BEAR_SPAWN_EGG, "&aPolar Bear Egg", lore26);	
		
		List<String> lore27 = new ArrayList<String>();
		lore27.add(ChatColor.translateAlternateColorCodes('&', "&e&oBuy Price: &a$500"));
		ItemStack item27 = createItem(Material.LLAMA_SPAWN_EGG, "&aLlama Egg", lore27);	
		
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Shop GUI");
		
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			sellItem(player, 125000, "creeper");
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			sellItem(player, 70000, "skeleton");
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			sellItem(player, 75000, "zombie");
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			sellItem(player, 50000, "spider");
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			sellItem(player, 50000, "cave_spider");
		});
		ClickableItem item5Click = ClickableItem.of(item5, e -> {
			sellItem(player, 200000, "blaze");
		});
		ClickableItem item6Click = ClickableItem.of(item6, e -> {
			sellItem(player, 125000, "pig_zombie");
		});
		ClickableItem itemEndClick = ClickableItem.of(itemend, e -> {
			sellItem(player, 500000, "enderman");
		});
		ClickableItem itemWitchClick = ClickableItem.of(itemswitch, e -> {
			sellItem(player, 75000, "witch");
		});
		ClickableItem itemSlimeClick = ClickableItem.of(itemslime, e -> {
			sellItem(player, 1000000, "slime");
		});
		ClickableItem itemGolem = ClickableItem.of(itemgolem, e -> {
			sellItem(player, 3000000, "iron_golem");
		});
		
		
		ClickableItem item9Click = ClickableItem.of(item9, e -> { 
			sellItem(player, 15000, "pig");
		});
		ClickableItem item10Click = ClickableItem.of(item10, e -> {
			sellItem(player, 25000, "cow");
		});
		ClickableItem item11Click = ClickableItem.of(item11, e -> {
			sellItem(player, 20000, "sheep");
		});
		ClickableItem item12Click = ClickableItem.of(item12, e -> {
			sellItem(player, 15000, "chicken");
		});
		ClickableItem item13Click = ClickableItem.of(item13, e -> {
			sellItem(player, 25000, "mushroom_cow");
		});
		ClickableItem item14Click = ClickableItem.of(item14, e -> {
			sellItem(player, 50000, "horse");
		});
		ClickableItem item15Click = ClickableItem.of(item15, e -> {
			sellItem(player, 500000, "magma_cube");
		});
		

		ClickableItem item16Click = ClickableItem.of(item16, e -> {
			sellItem(player, 500, new ItemStack(Material.VILLAGER_SPAWN_EGG), 1);
		});
		ClickableItem item17Click = ClickableItem.of(item17, e -> {
			sellItem(player, 500, new ItemStack(Material.PIG_SPAWN_EGG), 1);
		});		
		ClickableItem item18Click = ClickableItem.of(item18, e -> {
			sellItem(player, 400, new ItemStack(Material.SHEEP_SPAWN_EGG), 1);
		});		
		ClickableItem item19Click = ClickableItem.of(item19, e -> {
			sellItem(player, 400, new ItemStack(Material.COW_SPAWN_EGG), 1);
		});	
		ClickableItem item20Click = ClickableItem.of(item20, e -> {
			sellItem(player, 400, new ItemStack(Material.CHICKEN_SPAWN_EGG), 1);
		});
		ClickableItem item21Click = ClickableItem.of(item21, e -> {
			sellItem(player, 250, new ItemStack(Material.TROPICAL_FISH_SPAWN_EGG), 1);
		});
		ClickableItem item22Click = ClickableItem.of(item22, e -> {
			sellItem(player, 200, new ItemStack(Material.RABBIT_SPAWN_EGG), 1);
		});
		ClickableItem item23Click = ClickableItem.of(item23, e -> {
			sellItem(player, 1000, new ItemStack(Material.HORSE_SPAWN_EGG), 1);
		});		
		ClickableItem item24Click = ClickableItem.of(item24, e -> {
			sellItem(player, 200, new ItemStack(Material.CAT_SPAWN_EGG), 1);
		});
		ClickableItem item25Click = ClickableItem.of(item25, e -> {
			sellItem(player, 500, new ItemStack(Material.PANDA_SPAWN_EGG), 1);
		});
		ClickableItem item26Click = ClickableItem.of(item26, e -> {
			sellItem(player, 500, new ItemStack(Material.POLAR_BEAR_SPAWN_EGG), 1);
		});
		ClickableItem item27Click = ClickableItem.of(item27, e -> {
			sellItem(player, 500, new ItemStack(Material.LLAMA_SPAWN_EGG), 1);
		});

		cont.set(0, 0, itemWitchClick);
		cont.set(0, 1, item4Click);
		cont.set(0, 2, item0Click);
		cont.set(0, 3, item1Click);
		cont.set(0, 4, item2Click);
		cont.set(0, 5, item3Click);
		cont.set(0, 6, item5Click);
		cont.set(0, 7, item6Click);
		cont.set(0, 8, itemSlimeClick);		

		cont.set(1, 0, itemEndClick);
		cont.set(1, 1, item13Click);
		cont.set(1, 2, item9Click);
		cont.set(1, 3, item10Click);
		cont.set(1, 4, item11Click);
		cont.set(1, 5, item12Click);	
		cont.set(1, 6, item14Click);
		cont.set(1, 7, item15Click);
		cont.set(1, 8, itemGolem);
		

		cont.set(3, 1, item23Click); 
		cont.set(3, 2, item16Click); 
		cont.set(3, 3, item17Click); 
		cont.set(3, 4, item18Click); 
		cont.set(3, 5, item19Click);
		cont.set(3, 6, item20Click); 
		cont.set(3, 7, item21Click);
		cont.set(4, 2, item22Click); 
		cont.set(4, 3, item24Click); 
		cont.set(4, 4, item25Click); 
		cont.set(4, 5, item26Click); 
		cont.set(4, 6, item27Click); 
		cont.set(5, 4, ClickableItem.of(gui, e -> ShopNav.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
