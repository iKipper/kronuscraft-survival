package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class WarpInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	enum Warps {
		GREENLAND (
				"&2&lGreenland",
				"&aEasy", 
				new String [] { "", "&7Vanilla world, recommended for ", "&7starting members & farming!", "", "&aRECOMMENDED FOR NEW PLAYERS" },
				"world", 
				Material.GRASS_BLOCK,
				new int[] {1,1}
			),
		UNDERWORLD (
				"&c&lUnderworld",
				"&eMedium",
				new String [] { "", "&7Lava filled world, bring many", "&7supplies and equipment!" },
				"world_nether", 
				Material.LAVA_BUCKET,
				new int[] {1,3}
		),	
		OVERWORLD (
				"&6&lOverworld",
				"&cHard",
				new String [] { "", "&7An empty void world said", "&7to contain mysterious creatures!" },
				"world_the_end", 
				Material.END_STONE,
				new int[] {1,5}
		),		
		EMATHIA (
				"&e&lCOMING SOON",
				"&e---",
				new String [] { "", "&7A custom biome world with", "&7Bosses & hard mobs!", "", "&cNOT RECOMMENDED FOR NEW PLAYERS" },
				"Emathia", 
				Material.SUNFLOWER,
				new int[] {1,7}
		);

		public final String name;
		public final String difficulty;
		public final String[] loreLines;
		public final String command;
		public final Material guiObject;
		public final int[] containerCoordinates;

		Warps(String title, String difficulty, String[] loreLines, String command, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.difficulty = difficulty;
			this.loreLines = loreLines;
			this.command = command;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}

	}
	
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("warp").provider(new WarpInventory())
			.size(3, 9).title(ChatColor.GREEN + "World GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fill(ClickableItem.empty(glass));
		
		for(Warps shop : Warps.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7&m+------&r " + shop.difficulty + " &7&m------+"));
			for(String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = createItem(shop.guiObject, ChatColor.translateAlternateColorCodes('&', shop.name), lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				if(!player.getWorld().getName().equals(shop.command)) {
					player.teleport(new Location(Bukkit.getWorld(shop.command), 0.5, 256, 0));
				}
				Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						player.performCommand("wild " + shop.command);
					}
				}, 10L);
			});
			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Help GUI");
		cont.set(4, 4, ClickableItem.of(gui, e -> HelpInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
	
	}
}
